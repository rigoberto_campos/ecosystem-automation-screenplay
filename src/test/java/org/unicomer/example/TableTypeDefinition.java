package org.unicomer.example;

import io.cucumber.java.DataTableType;
import org.unicomer.models.Customer;
import org.unicomer.models.Request;

import java.util.Map;

public class TableTypeDefinition {

    @DataTableType(replaceWithEmptyString = "novalor")
    public Customer createCustomer(Map<String, String> entry){
            return new Customer(entry);
    }

    @DataTableType(replaceWithEmptyString = "novalor")
    public Request createNewRequest(Map<String, String> entry){
        return new Request(entry);
    }
}
