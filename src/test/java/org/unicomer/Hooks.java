package org.unicomer;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.unicomer.constants.SystemConstants;
import org.unicomer.utils.DriverManager;
import org.unicomer.utils.UtilsManager;

public class Hooks {

    @Before("@WFTimeMonitoring")
    public void setCredentialsWFAdmin(){
        SystemConstants.WORKF_USERNAME = "WORKF.ADMINUSER";
        SystemConstants.WORKF_PASSWORD = "WORKF.ADMINPASSWORD";
    }

    @Before("@RequestTracking")
    public void setCredentialsWFAnalyst(){
        SystemConstants.WORKF_USERNAME = "WORKF.ANALYSTUSER";
        SystemConstants.WORKF_PASSWORD = "WORKF.ANALYSTPASS";
    }
    @Before
    public void setup() {
        Actor admin = Actor.named("admin");
        admin.can(BrowseTheWeb.with(DriverManager.getInstance().getChromeDriver()));
        UtilsManager.getInstance().setActor(admin);
    }

    @After()
    public void closeBrowser() {
        DriverManager.getInstance().chromeOptions();
    }
}
