package org.unicomer.task.mambu.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class NavigateSubMenu implements Task {

    public static Target BTN_OPTION_SUBMENU;

    public NavigateSubMenu(String option){
        BTN_OPTION_SUBMENU = Target.the("OPTION SUBMENU").located(By.xpath("//div[contains(@id,'"+option+"')]"));
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_OPTION_SUBMENU));
    }
    public static NavigateSubMenu data(String option) {
        return Tasks.instrumented(NavigateSubMenu.class, option);
    }
}
