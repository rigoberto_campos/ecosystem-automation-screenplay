package org.unicomer.task.mambu.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.unicomer.userInterfaces.mambu.MambuInterfaz;

public class ConsultDocument implements Task {

    private static String documentId;
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(SendKeys.of(documentId).into(MambuInterfaz.INPUT_SEARCH));
        actor.attemptsTo(Click.on(MambuInterfaz.TR_CLIENT ));
    }
    public static ConsultDocument insert(String id) {
        documentId = id;
        return Tasks.instrumented(ConsultDocument.class);
    }
}
