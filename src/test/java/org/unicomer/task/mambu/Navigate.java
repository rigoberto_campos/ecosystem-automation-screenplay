package org.unicomer.task.mambu;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import org.unicomer.constants.SystemConstants;
import org.unicomer.userInterfaces.emma.EmmaCredentials;
import org.unicomer.userInterfaces.mambu.MambuInterfaz;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.Wait;

public class Navigate implements Task {

    private static String url;
    private static String username;
    private static String password;


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.url(url));
        actor.attemptsTo(Click.on(MambuInterfaz.BTN_START));
        actor.attemptsTo(Enter.theValue(username).into(EmmaCredentials.INPUT_USERNAME));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_SEND_CREDENTIAL));
        actor.attemptsTo(Enter.theValue(password).into(EmmaCredentials.INPUT_PASSWORD));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_SEND_CREDENTIAL));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_SESSION_ACTIVE));
        Wait.to(3);
    }

    public static Navigate page(String mambuPage) {
        ConstantReader.getInstance().setEnv(mambuPage);
        url = ConstantReader.getInstance().getProperties(SystemConstants.MAMBU_URL);
        username = ConstantReader.getInstance().getProperties(SystemConstants.MAMBU_USERNAME);
        password = ConstantReader.getInstance().getProperties(SystemConstants.MAMBU_PASSWORD);
        return Tasks.instrumented(Navigate.class);
    }
}
