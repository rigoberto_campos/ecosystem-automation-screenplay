package org.unicomer.task.workflow;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import org.unicomer.utils.SelectMenuList;
import org.unicomer.utils.Wait;

public class SelectMenu implements Task {
    private static String menuName;
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(SelectMenuList.since(menuName));
        Wait.to(7);
    }

    public static org.unicomer.task.workflow.SelectMenu page(String menu) {
        menuName = menu;
        return Tasks.instrumented(org.unicomer.task.workflow.SelectMenu.class);
    }
}
