package org.unicomer.task.workflow;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.unicomer.constants.SystemConstants;
import org.unicomer.interactions.SelectList;
import org.unicomer.questions.LoadingElement;
import org.unicomer.userInterfaces.workflow.WorkflowHomePage;
import org.unicomer.userInterfaces.workflow.WorkflowRequestTracking;
import org.unicomer.utils.ConstantReader;

public class DataValidationClient implements Task {
    private static Target BTN_SAVE;
    @Override
    public <T extends Actor> void performAs(T actor) {
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(BTN_SAVE));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(SelectList.since(WorkflowRequestTracking.LIST_GENERAL_INFO,WorkflowRequestTracking.OPTION_MENU," Identifications "));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.LIST_GENERAL_INFO));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.SELECT_RESOLVE_IDENTIFICATIONS).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(Click.on(BTN_SAVE));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(SelectList.since(WorkflowRequestTracking.LIST_GENERAL_INFO,WorkflowRequestTracking.OPTION_MENU," Addresses "));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.LIST_GENERAL_INFO));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.SELECT_RESOLVE_ADDRESSES).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(Click.on(BTN_SAVE));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(SelectList.since(WorkflowRequestTracking.LIST_GENERAL_INFO,WorkflowRequestTracking.OPTION_MENU," Phones "));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.LIST_GENERAL_INFO));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.SELECT_RESOLVE_PHONES).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(Click.on(BTN_SAVE));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(SelectList.since(WorkflowRequestTracking.LIST_GENERAL_INFO,WorkflowRequestTracking.OPTION_MENU," Financial information "));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.LIST_GENERAL_INFO));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.SELECT_RESOLVE_FINANCIAL_INFORMATION).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(Click.on(BTN_SAVE));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(SelectList.since(WorkflowRequestTracking.LIST_GENERAL_INFO,WorkflowRequestTracking.OPTION_MENU," References "));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.LIST_GENERAL_INFO));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.SELECT_RESOLVE_REFERENCES).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(Click.on(BTN_SAVE));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
    }
    public static org.unicomer.task.workflow.DataValidationClient page(String trn) {
        String country = ConstantReader.getInstance().getProperties(SystemConstants.COUNTRY);
        if(country.equals("Aruba")){
             BTN_SAVE = WorkflowRequestTracking.BTN_SAVE_DATA;
        }else {
             BTN_SAVE = WorkflowRequestTracking.BTN_SAVE_DATA_JM;
        }
        return Tasks.instrumented(org.unicomer.task.workflow.DataValidationClient.class,trn);
    }

}
