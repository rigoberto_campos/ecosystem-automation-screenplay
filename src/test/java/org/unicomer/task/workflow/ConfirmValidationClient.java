package org.unicomer.task.workflow;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.unicomer.interactions.SelectList;
import org.unicomer.questions.LoadingElement;
import org.unicomer.userInterfaces.workflow.WorkflowHomePage;
import org.unicomer.userInterfaces.workflow.WorkflowRequestTracking;
import org.unicomer.utils.Wait;

public class ConfirmValidationClient implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(SelectList.since(WorkflowRequestTracking.LIST_GENERAL_INFO,WorkflowRequestTracking.OPTION_MENU," Resolve "));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.LIST_GENERAL_INFO).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        //actor.attemptsTo(Click.on(WorkflowRequestTracking.SELECT_RESOLVE).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_STATUS).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_DATA_VERIFICATION).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SUB_STATUS).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_DATA_VALIDATION).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SAVE_CHANGE).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_STATUS).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SELECT_APPROVED).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SAVE_CHANGE).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        Wait.to(10);

    }

    public static org.unicomer.task.workflow.ConfirmValidationClient page() {
        return Tasks.instrumented(org.unicomer.task.workflow.ConfirmValidationClient.class);
    }
}
