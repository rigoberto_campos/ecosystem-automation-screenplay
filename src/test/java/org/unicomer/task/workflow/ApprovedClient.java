package org.unicomer.task.workflow;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.workflow.WorkflowRequestTracking;

public class ApprovedClient implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_STATUS));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SELECT_APPROVED));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SAVE_CHANGE));
    }

    public static org.unicomer.task.workflow.ApprovedClient page(String id) {

        return Tasks.instrumented(org.unicomer.task.workflow.ApprovedClient.class);
    }
}
