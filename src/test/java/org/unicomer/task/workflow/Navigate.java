package org.unicomer.task.workflow;


import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import org.unicomer.constants.SystemConstants;
import org.unicomer.userInterfaces.workflow.WorkflowlLogin;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.Wait;

public class Navigate implements Task {
    public Navigate(String pageWorkf) {
        this.pageWorkf=pageWorkf;
    }
    private static String pageWorkf;
    private static String username;
    private static String password;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.url(pageWorkf));
        actor.attemptsTo(Enter.theValue(username).into(WorkflowlLogin.INPUT_USERNAME));
        actor.attemptsTo(Enter.theValue(password).into(WorkflowlLogin.INPUT_PASSWORD));
        actor.attemptsTo(Click.on(WorkflowlLogin.BTN_CREDENTIALS));
        Wait.to(3);
    }

    public static org.unicomer.task.workflow.Navigate page(String pageWorkf) {
        ConstantReader.getInstance().setEnv(pageWorkf);
        pageWorkf = ConstantReader.getInstance().getProperties(SystemConstants.WORKF_URL);
        username = ConstantReader.getInstance().getProperties(SystemConstants.WORKF_USERNAME);
        password = ConstantReader.getInstance().getProperties(SystemConstants.WORKF_PASSWORD);
        return Tasks.instrumented(org.unicomer.task.workflow.Navigate.class,pageWorkf);
    }

}
