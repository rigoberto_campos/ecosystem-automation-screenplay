package org.unicomer.task.workflow;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.unicomer.constants.SystemConstants;
import org.unicomer.interactions.SelectList;
import org.unicomer.userInterfaces.workflow.WorkflowHomePage;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.Wait;

public class SelectCountry implements Task {
    public static String country ;
    public static String language ;
    @Override
    public <T extends Actor> void performAs(T actor) {

        //actor.attemptsTo(SelectList.since(WorkflowHomePage.LIST_COUNTRY,WorkflowHomePage.LIST_ELEMENTS,country));
        actor.attemptsTo(SelectList.since(WorkflowHomePage.LIST_LANGUAGE,WorkflowHomePage.LIST_ELEMENTS,language));
    }

    public static org.unicomer.task.workflow.SelectCountry the() {
        language = ConstantReader.getInstance().getProperties(SystemConstants.LANGUAGE);
        country = ConstantReader.getInstance().getProperties(SystemConstants.COUNTRY);
        return Tasks.instrumented(org.unicomer.task.workflow.SelectCountry.class);
    }
}
