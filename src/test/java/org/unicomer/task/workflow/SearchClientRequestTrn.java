package org.unicomer.task.workflow;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.constants.SystemConstants;
import org.unicomer.questions.LoadingElement;
import org.unicomer.userInterfaces.workflow.WorkflowHomePage;
import org.unicomer.userInterfaces.workflow.WorkflowRequestTracking;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.Wait;

public class SearchClientRequestTrn implements Task {
    private static String userTrn;
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(userTrn).into(WorkflowRequestTracking.INPUT_TRN));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SEARCH_TRN).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_CLICK_REQUEST).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
    }

    public static org.unicomer.task.workflow.SearchClientRequestTrn page(String trn) {
        userTrn = trn;
        return Tasks.instrumented(org.unicomer.task.workflow.SearchClientRequestTrn.class,trn);
    }
}
