package org.unicomer.task.workflow;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.workflow.WorkflowRequestTracking;

public class AccountAllRequest implements Task {
    private static String documentId;
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SELECT_ALL_REQUEST));
        actor.attemptsTo(Enter.theValue(documentId).into(WorkflowRequestTracking.INPUT_SEARCH_DOCUMENT_REQUEST));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_SEARCH));
        actor.attemptsTo(Click.on(WorkflowRequestTracking.BTN_REQUEST_NUMBER));

    }

    public static org.unicomer.task.workflow.ConfirmValidationClient page(String id) {
        documentId = id;
        return Tasks.instrumented(org.unicomer.task.workflow.ConfirmValidationClient.class,id);
    }
}
