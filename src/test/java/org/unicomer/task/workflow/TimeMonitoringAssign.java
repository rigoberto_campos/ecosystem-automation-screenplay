package org.unicomer.task.workflow;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.actions.selectactions.SelectByValueFromTarget;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.ui.Dropdown;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.serenitybdd.screenplay.waits.WaitUntilTargetIsReady;
import org.unicomer.questions.LoadingElement;
import org.unicomer.userInterfaces.workflow.WorkflowHomePage;
import org.unicomer.userInterfaces.workflow.WorkflowTimeMonitoring;
import org.unicomer.utils.NewClickWorkflow;
import org.unicomer.utils.Wait;


public class TimeMonitoringAssign implements Task {
    private static String userTrn;
    @Override
    public <T extends Actor> void performAs(T actor) {
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(JavaScriptClick.on(WorkflowTimeMonitoring.BTN_SEARCH_BY_DOCUMENT));
        actor.attemptsTo(JavaScriptClick.on(WorkflowTimeMonitoring.BTN_SELECT_DOCUMENT));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Enter.theValue(userTrn).into(WorkflowTimeMonitoring.INPUT_TRN));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(JavaScriptClick.on(WorkflowTimeMonitoring.BTN_SEARCH_TRN));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(WorkflowTimeMonitoring.BTN_SELECT_REQUEST).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(WorkflowTimeMonitoring.BTN_ASSIGN_REQUEST));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(JavaScriptClick.on(WorkflowTimeMonitoring.BTN_SELECT_USER));
        LoadingElement.isVisible(WorkflowHomePage.SPINNER).answeredBy(actor);
        actor.attemptsTo(Click.on(WorkflowTimeMonitoring.BTN_CONFIRM).withStrategy(ClickStrategy.WAIT_UNTIL_ENABLED));
        Wait.to(10);

    }

    public static org.unicomer.task.workflow.TimeMonitoringAssign page(String trn) {
        userTrn = trn;
        return Tasks.instrumented(org.unicomer.task.workflow.TimeMonitoringAssign.class,trn);
    }
}
