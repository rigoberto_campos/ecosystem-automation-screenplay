package org.unicomer.task.services.cashLoan;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.unicomer.utils.UtilsManager;

import java.util.HashMap;

public class GetTokenTest2 implements Task {
    private String URL_BASE = "http://10.192.27.58";
    private String path = "/courts.net.ws/WCustomerManager.asmx";

    HashMap header;
    public GetTokenTest2() {
        this.header = new HashMap<>();
        header.put("Expect","100-continue");
        header.put("SOAPAction","http://strategicthought.com/webservices/CustomerSearch");
        header.put("Content-Type","text/xml; charset=utf-8");
        header.put("Accept-Encoding","gzip");
        header.put("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 2.0.50727.9168)");

    }

    public <T extends Actor> void performAs(T actor) {

        actor.can(CallAnApi.at(URL_BASE));
        actor.attemptsTo(
                Post.to(path).with(req -> req.headers(header).
                        body("<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Header><Authentication xmlns=\"http://strategicthought.com/webservices/\"><User>rigoberto.campos</User><UserId>9905502</UserId><Cookie>.CoSaCS=55153D6C3516A990FA985C11DA46C6EE1457C8B408B764C583F5647D60A7A1BE4605DA8FE34C4DEDC9C868601110B024F691A19B97A60415516CC4C87A865B682FD04B8D0DCD3B1F99266DF73A693C4444583B50BB681C4E1EAA9FF11305ED50543DB5F4; path=/; HttpOnly; SameSite=Lax</Cookie><Password>12345678**1*$</Password><Culture>en-JM</Culture><Country>J</Country><Version>10.7.4.12</Version></Authentication></soap:Header><soap:Body><CustomerSearch xmlns=\"http://strategicthought.com/webservices/\"><customerID /><firstName>Jhon</firstName><lastName /><address /><phoneNumber /><limit>250</limit><settled>0</settled><exactMatch>true</exactMatch><storeType>%</storeType><source>CUSTSRCH</source></CustomerSearch></soap:Body></soap:Envelope>"
                            )
                )
        );

        String res = SerenityRest.lastResponse().getBody().asPrettyString();
        String custId = res.substring(res.indexOf("<Customer_x0020_ID>") + 19, res.indexOf("</Customer_x0020_ID>"));
        System.out.println(custId + " <---------------------------------------------------------");
        UtilsManager.getInstance().addRestVaribles("${custid}", custId);

        //SerenityRest.lastResponse().prettyPeek();
        //System.out.println(newBody);
        //String res = SerenityRest.lastResponse().getBody().asPrettyString();
        //System.out.println(res);

    }

    public static GetTokenTest2 getInformation() {
        return Tasks.instrumented(GetTokenTest2.class);
    }
}
