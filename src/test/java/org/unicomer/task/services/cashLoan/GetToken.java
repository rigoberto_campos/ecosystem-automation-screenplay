package org.unicomer.task.services.cashLoan;

import io.restassured.path.json.JsonPath;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.unicomer.utils.UtilsManager;

import java.util.HashMap;

public class GetToken implements Task {
    private String URL_BASE = "http://10.192.27.58";
    private String path = "/courts.net.ws/servicestack.ashx/json/syncreply/Blue.Cosacs.Shared.Services.Credit.CashLoanQualificationRequest";

    HashMap header;
    public GetToken() {
        this.header = new HashMap<>();
        header.put("Expect","100-continue");
        header.put("Content-Type","application/json");
        header.put("X-Cosacs-Version","10.7.4.12");
        header.put("X-Cosacs-User","rigoberto.campos");
        header.put("X-Cosacs-Password","12345678**1*$");
        header.put("X-Cosacs-Culture","en-JM");
        header.put("Accept","application/json, */*");
        header.put("X-Cosacs-CountryCode","J");
    }

    public <T extends Actor> void performAs(T actor) {

        //connectioSQLServer();

        actor.can(CallAnApi.at(URL_BASE));
        String body = UtilsManager.getInstance().readBodyFile("src\\test\\resources\\files\\bodyServicesRest\\cashLoan\\body1.json");
        String newBody = UtilsManager.getInstance().replaceBodyVar(body);
        actor.attemptsTo(
                Post.to(path).with(req -> req.headers(header).
                        body(newBody))
        );

        SerenityRest.lastResponse().prettyPeek();
        JsonPath json = SerenityRest.lastResponse().jsonPath();
        UtilsManager.getInstance().addRestVaribles("${StampDuty}", json.get("StampDuty").toString());
        UtilsManager.getInstance().addRestVaribles("${LoanAmount}", json.get("Cashloan.LoanAmount").toString());
        UtilsManager.getInstance().addRestVaribles("${Term}", json.get("Cashloan.Term").toString());
        UtilsManager.getInstance().addRestVaribles("${AdminCharge}", json.get("Cashloan.AdminCharge").toString());
        UtilsManager.getInstance().addRestVaribles("${accountNo}", json.get("Cashloan.AcctNo").toString());

    }

    public static GetToken getInformation() {
        return Tasks.instrumented(GetToken.class);
    }
}

