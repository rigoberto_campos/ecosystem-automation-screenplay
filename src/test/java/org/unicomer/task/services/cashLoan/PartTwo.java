package org.unicomer.task.services.cashLoan;

import io.restassured.path.json.JsonPath;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.unicomer.utils.UtilsManager;

import java.util.HashMap;

public class PartTwo implements Task {
    private String URL_BASE = "http://10.192.27.58";
    private String path = "/courts.net.ws/WAccountManager.asmx";

    HashMap header;
    public PartTwo() {
        this.header = new HashMap<>();
        header.put("Expect","100-continue");
        header.put("SOAPAction","http://strategicthought.com/webservices/CalculateCashLoanTerms");
        header.put("Content-Type","text/xml; charset=utf-8");
        header.put("Accept-Encoding","gzip");
        header.put("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 2.0.50727.9168)");

    }

    public <T extends Actor> void performAs(T actor) {

        actor.can(CallAnApi.at(URL_BASE));
        String body ="<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Header><Authentication xmlns=\"http://strategicthought.com/webservices/\"><User>${username}</User><UserId>9905502</UserId><Cookie>.CoSaCS=03C61D61F86F9C58EA851265CD81C996CCF2CAB886EC912DC48243C19EBC29A61C28A482F3EDC9CE8E2C6D190A29FE9AFC294A45AB002776D9A0FCD02BA4235A3CE2145DAD72DE3FCF105E01E5253B7B0ABD4E6A044FA5A7FC5EB8F0B33E65ACD6851E85; path=/; HttpOnly; SameSite=Lax</Cookie><Password>${password}</Password><Culture>en-JM</Culture><Country>J</Country><Version>10.7.4.12</Version></Authentication></soap:Header><soap:Body><CalculateCashLoanTerms xmlns=\"http://strategicthought.com/webservices/\"><countryCode>J</countryCode><det><loanAmount>${LoanAmount}</loanAmount><term>${Term}</term><instalment>0</instalment><finInstal>0</finInstal><serviceChg>0</serviceChg><insuranceChg>0</insuranceChg><adminChg>2500.0000</adminChg><agreementTotal>0</agreementTotal><accountNo>${accountNo}</accountNo><custId>${custid}</custId><termsType>C2</termsType><scoreBand>B</scoreBand><taxRate>15</taxRate><insuranceTax>0</insuranceTax><adminTax>0</adminTax><loanStatus> </loanStatus><custName>Rigoberto Campos</custName><empeenoAccept>99999</empeenoAccept><empeenoDisburse>99999</empeenoDisburse><transrefno>0</transrefno><outstBal>0</outstBal><firstInstalDate>0001-01-01T00:00:00</firstInstalDate><dateprop>0001-01-01T00:00:00</dateprop><datePrinted>2023-07-17T14:10:20.46</datePrinted><stampDuty>20.0000</stampDuty><waiveAdminCharge>true</waiveAdminCharge><empeenoAdminChargeWaived xsi:nil=\"true\" /><empeenoLoanAmountChanged>0</empeenoLoanAmountChanged></det><branchNo>910</branchNo><calledFromCashLoanPopulateScreen>true</calledFromCashLoanPopulateScreen><isMambuAccount>true</isMambuAccount></CalculateCashLoanTerms></soap:Body></soap:Envelope>";
        String newBody = UtilsManager.getInstance().replaceBodyVar(body);
        actor.attemptsTo(
                Post.to(path).with(req -> req.headers(header).
                        body(newBody)
                )
        );
        String res = SerenityRest.lastResponse().getBody().asPrettyString();

        String First_instalment = res.substring(res.indexOf("<instalment>") + 12, res.indexOf("</instalment>"));
        String Final_instalment = res.substring(res.indexOf("<finInstal>") + 11, res.indexOf("</finInstal>"));
        String Service_Chrg = res.substring(res.indexOf("<serviceChg>") + 12, res.indexOf("</serviceChg>"));
        String Agreement_Total = res.substring(res.indexOf("<agreementTotal>") + 16, res.indexOf("</agreementTotal>"));
        String tax_rate = res.substring(res.indexOf("<taxRate>") + 9, res.indexOf("</taxRate>"));
        String admin_tax = res.substring(res.indexOf("<adminTax>") + 10, res.indexOf("</adminTax>"));

        UtilsManager.getInstance().addRestVaribles("${First_instalment}", First_instalment);
        UtilsManager.getInstance().addRestVaribles("${Final_instalment}", Final_instalment);
        UtilsManager.getInstance().addRestVaribles("${Service_Chrg}", Service_Chrg);
        UtilsManager.getInstance().addRestVaribles("${Agreement_Total}", Agreement_Total);
        UtilsManager.getInstance().addRestVaribles("${tax_rate}", tax_rate);
        UtilsManager.getInstance().addRestVaribles("${admin_tax}", admin_tax);

    }

    public static PartTwo getInformation() {
        return Tasks.instrumented(PartTwo.class);
    }
}
