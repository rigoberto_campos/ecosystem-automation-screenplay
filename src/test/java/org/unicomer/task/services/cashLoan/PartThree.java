package org.unicomer.task.services.cashLoan;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.unicomer.utils.UtilsManager;

import java.util.HashMap;

public class PartThree implements Task {
    private String URL_BASE = "http://10.192.27.58";
    private String path = "/courts.net.ws/WAccountManager.asmx";

    HashMap header;
    public PartThree() {
        this.header = new HashMap<>();
        header.put("Expect","100-continue");
        header.put("SOAPAction","http://strategicthought.com/webservices/CashLoanDeliverAccount");
        header.put("Content-Type","text/xml; charset=utf-8");
        header.put("Accept-Encoding","gzip");
        header.put("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 2.0.50727.9168)");

    }

    public <T extends Actor> void performAs(T actor) {

        actor.can(CallAnApi.at(URL_BASE));
        String body ="<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Header><Authentication xmlns=\"http://strategicthought.com/webservices/\"><User>${username}</User><UserId>9905502</UserId><Cookie>.CoSaCS=D5E48DFE02DCB433F44D8FB7CDA75D6BA8117116DD67A19E660005DAADF79EACB6377EA8050DFF7F3214881D6478FD0E1648E82A9F98A86456D7A006294E7F8BB90342A59E3D4A1A477BFD8E8D0260DF2BCAB053D98CA14568C05A29D5E226CF6B4FBFBE; path=/; HttpOnly; SameSite=Lax</Cookie><Password>${password}</Password><Culture>en-JM</Culture><Country>J</Country><Version>10.7.4.12</Version></Authentication></soap:Header><soap:Body><CashLoanDeliverAccount xmlns=\"http://strategicthought.com/webservices/\"><det><loanAmount>${LoanAmount}</loanAmount><term>${Term}</term><instalment>${First_instalment}</instalment><finInstal>${Final_instalment}</finInstal><serviceChg>${Service_Chrg}</serviceChg><insuranceChg>0.00</insuranceChg><adminChg>${AdminCharge}</adminChg><agreementTotal>${Agreement_Total}</agreementTotal><accountNo>${accountNo}</accountNo><custId>${custid}</custId><termsType>C2</termsType><scoreBand>B</scoreBand><taxRate>${tax_rate}</taxRate><insuranceTax>0</insuranceTax><adminTax>${admin_tax}</adminTax><loanStatus>D</loanStatus><custName>Rigoberto Campos</custName><empeenoAccept>99999</empeenoAccept><empeenoDisburse>9905502</empeenoDisburse><transrefno>0</transrefno><outstBal>0</outstBal><firstInstalDate>0001-01-01T00:00:00</firstInstalDate><dateprop>0001-01-01T00:00:00</dateprop><datePrinted>2023-07-17T14:10:20.46</datePrinted><stampDuty>${StampDuty}</stampDuty><waiveAdminCharge>true</waiveAdminCharge><empeenoAdminChargeWaived xsi:nil=\"true\" /><empeenoLoanAmountChanged>0</empeenoLoanAmountChanged></det><branchNo>892</branchNo><CashLoanDisbursementDet><accountNo>${accountNo}</accountNo><custId>${custid}</custId><loanAmount>${LoanAmount}</loanAmount><disbursementType>1</disbursementType></CashLoanDisbursementDet></CashLoanDeliverAccount></soap:Body></soap:Envelope>";
        String newBody = UtilsManager.getInstance().replaceBodyVar(body);
        actor.attemptsTo(
                Post.to(path).with(req -> req.headers(header).
                        body(newBody)
                )
        );

        SerenityRest.lastResponse().prettyPeek();
        System.out.println(newBody);
        String res = SerenityRest.lastResponse().getBody().asPrettyString();
        System.out.println(res);

    }

    public static PartThree getInformation() {
        return Tasks.instrumented(PartThree.class);
    }
}