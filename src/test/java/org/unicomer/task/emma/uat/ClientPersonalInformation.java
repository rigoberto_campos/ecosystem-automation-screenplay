package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.constants.SystemConstants;
import org.unicomer.models.Customer;
import org.unicomer.interactions.SelectList;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
import org.unicomer.utils.ConstantReader;

public class ClientPersonalInformation implements Task {

    Customer customer;
    public ClientPersonalInformation(Customer customer){
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {


        actor.attemptsTo(SelectList.since(EmmaUatHome.DRPD_SELECT_GENDER,EmmaUatHome.DRPD_SELECT_PANEL, customer.getGender()));
        actor.attemptsTo(Enter.theValue(ConstantReader.getInstance().getProperties(SystemConstants.COUNTRY)).into(EmmaUatHome.INPUT_COUNTRY));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_CNTRY_BIRD,EmmaUatHome.DRP_CNTRY_PANEL, ConstantReader.getInstance().getProperties(SystemConstants.COUNTRY)));
        actor.attemptsTo(Enter.theValue(ConstantReader.getInstance().getProperties(SystemConstants.COUNTRY)).into(EmmaUatHome.INPUT_COUNTRY));
        actor.attemptsTo(Enter.theValue(customer.getDependsNumber()).into(EmmaUatHome.INPUT_NUMBER_DEPENDS));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_MARRIED_STATUS,EmmaUatHome.DRPD_SELECT_PANEL, customer.getMarriedStatus()));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_PAYMENT_FRENQUENCY,EmmaUatHome.DRPD_SELECT_PANEL, customer.getFrecuencyPaid()));
        actor.attemptsTo(Enter.theValue(customer.getIncome()).into(EmmaUatHome.INPUT_PAYMENT_INCOME));
        actor.attemptsTo(Enter.theValue(customer.getAddress()).into(EmmaUatHome.TXT_ADRRES));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_SEND_FORM));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_CONFIRM_AMOUNT));
    }

    public static ClientPersonalInformation data(Customer customer) {

        return Tasks.instrumented(ClientPersonalInformation.class, customer);
    }
}
