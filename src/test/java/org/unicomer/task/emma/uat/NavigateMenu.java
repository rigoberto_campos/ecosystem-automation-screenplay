package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class NavigateMenu implements Task {

    public static Target BTN_CREDIT_APPLICATION;

    public NavigateMenu(String menu){
        BTN_CREDIT_APPLICATION = Target.the("CHOSE MENU").located(By.xpath("(//span[contains(text(),'"+menu+"')])[2]"));
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(BTN_CREDIT_APPLICATION));
    }

    public static NavigateMenu data(String menu) {

        return Tasks.instrumented(NavigateMenu.class, menu);
    }
}
