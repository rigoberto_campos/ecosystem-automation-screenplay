package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.WriteInto;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.interactions.SelectList;

public class ClientBasicInfo implements Task {

    Customer customer;

    public ClientBasicInfo(Customer customer){
        this.customer = customer;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(WriteInto.theInput(EmmaInterfazBasicInformation.INPUT_NAME,customer.getFirstName()));
        actor.attemptsTo(Enter.theValue(customer.getSecondName()).into(EmmaInterfazBasicInformation.INPUT_SECOND_NAME));
        actor.attemptsTo(Enter.theValue(customer.getFirstLastName()).into(EmmaInterfazBasicInformation.INPUT_LAST_NAME));
        actor.attemptsTo(Enter.theValue(customer.getSecondLastName()).into(EmmaInterfazBasicInformation.INPUT_SECOND_LAST_NAME));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaInterfazBasicInformation.INPUT_PHONE_NUMBER_JM));
        actor.attemptsTo(Enter.theValue(customer.getEmail()).into(EmmaInterfazBasicInformation.INPUT_EMAIL));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_BIRTH_DATE_JM));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SELECT_YEARS));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_BIRTH_DATE_JM,customer.getYear(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_BIRTH_DATE_JM,customer.getMonth(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_BIRTH_DATE_JM,customer.getDay(), SelectList.getDate()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SEND_EVALUATE));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_PREAPPROVED_OK));
    }

    public static ClientBasicInfo data(Customer customer) {

        return Tasks.instrumented(ClientBasicInfo.class, customer);
    }
}
