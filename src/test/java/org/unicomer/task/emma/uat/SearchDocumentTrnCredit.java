package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectList;
import org.unicomer.userInterfaces.emma.EmmaCreditApplicationStatus;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

public class SearchDocumentTrnCredit implements Task {

    String documentId;
    String typeRequest;

    public SearchDocumentTrnCredit(String documentId, String typeRequest){
        this.documentId = documentId;
        this.typeRequest = typeRequest;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Enter.theValue(documentId).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT_CLIENT));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.DRPD_TYPE_REQUEST));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,typeRequest, SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.BTN_CHECK_CLIENT_CREDIT));
    }
    public static SearchDocumentTrnCredit data(String documentId,String typeRequest) {

        return Tasks.instrumented(SearchDocumentTrnCredit.class, documentId, typeRequest);
    }
}
