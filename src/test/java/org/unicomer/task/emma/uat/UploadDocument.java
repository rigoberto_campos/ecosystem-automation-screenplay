package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.unicomer.questions.LoadingElement;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
import org.unicomer.userInterfaces.emma.EmmaUploadDocuments;
import org.unicomer.utils.UtilsManager;
import org.unicomer.utils.Wait;

public class UploadDocument implements Task {

    Customer customer;
    public UploadDocument(Customer customer){
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(EmmaUatHome.ICON_UPLOAD_DOCUMENT1));
        Wait.to(1);
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        LoadingElement.isVisible(EmmaUatHome.EMMA_WAIT_ELEMENT).answeredBy(actor);
        actor.attemptsTo(Click.on(EmmaUatHome.ICON_UPLOAD_DOCUMENT2));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        LoadingElement.isVisible(EmmaUatHome.EMMA_WAIT_ELEMENT).answeredBy(actor);
        actor.attemptsTo(Click.on(EmmaUatHome.ICON_UPLOAD_DOCUMENT3));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        LoadingElement.isVisible(EmmaUatHome.EMMA_WAIT_ELEMENT).answeredBy(actor);
        actor.attemptsTo(Click.on(EmmaUatHome.ICON_UPLOAD_DOCUMENT4));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        LoadingElement.isVisible(EmmaUatHome.EMMA_WAIT_ELEMENT).answeredBy(actor);
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_SEND_FORM));
        LoadingElement.isVisible(EmmaUatHome.EMMA_WAIT_ELEMENT).answeredBy(actor);
    }

    public static UploadDocument data(Customer customer) {

        return Tasks.instrumented(UploadDocument.class, customer);
    }
}
