package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import org.unicomer.interactions.SelectList;
import org.unicomer.userInterfaces.emma.EmmaCreditApplicationStatus;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;

public class SelectAvailablePromotion implements Task {

    private String selectPromotion;
    private String termMonths;
    private String store;
    private String csrNumber;

    public SelectAvailablePromotion(String selectPromotion, String termMonths, String store,String csrNumber){
        this.selectPromotion = selectPromotion;
        this.termMonths = termMonths;
        this.store = store;
        this.csrNumber = csrNumber;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.DRPD_TYPE_PROMOTION));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,selectPromotion, SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.DRPD_TYPE_TERM_MONTHS));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,termMonths, SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.DRPD_TYPE_STORE));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,store, SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(csrNumber).into(EmmaCreditApplicationStatus.CSR_NUMBER));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.BTN_CHECK_OUT));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.ALERT_CREDIT_APPLICATION));

    }

    public static SelectAvailablePromotion data(String selectPromotion, String termMonths, String store,String csrNumber) {
        return Tasks.instrumented(SelectAvailablePromotion.class,selectPromotion,termMonths,store,csrNumber);
    }
}
