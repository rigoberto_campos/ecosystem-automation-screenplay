package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
public class NewClientForm implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_NEW_CREDIT));
    }

    public static NewClientForm data() {

        return Tasks.instrumented(NewClientForm.class);
    }
}
