package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
import org.unicomer.interactions.SelectList;

public class SelectDocumentType implements Task {

    private String docType;

    public SelectDocumentType(String docType){
        this.docType = docType;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_DOCUMENT));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,docType, SelectList.getElement()));
    }

    public static SelectDocumentType data(String docType) {
        return Tasks.instrumented(SelectDocumentType.class, docType);
    }
}
