package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaUatHome;

public class ContactInformation implements Task {

    Customer customer;
    public ContactInformation(Customer customer){
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Enter.theValue(customer.getFirstName()).into(EmmaUatHome.INPUT_PERSONAL_REFERENCE_1));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaUatHome.INPUT_PHONE_REFERENCE_1));
        actor.attemptsTo(Enter.theValue(customer.getSecondName()).into(EmmaUatHome.INPUT_PERSONAL_REFERENCE_2));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaUatHome.INPUT_PHONE_REFERENCE_2));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_SEND_FORM));

    }

    public static ContactInformation data(Customer customer) {

        return Tasks.instrumented(ContactInformation.class, customer);
    }
}
