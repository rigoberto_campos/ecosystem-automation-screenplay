package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectRadioButton;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

public class NewLoanAmount implements Task {

    String amount;
    String addCpi;

    public NewLoanAmount(String amount,String addCpi) {
        this.amount = amount;
        this.addCpi = addCpi;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        //actor.attemptsTo(Scroll.to(EmmaUatLoanQuoter.INPUT_AMOUNT));
        actor.attemptsTo(Enter.theValue(amount).into(EmmaUatLoanQuoter.INPUT_AMOUNT));
        actor.attemptsTo(SelectRadioButton.with(addCpi));
        actor.attemptsTo(Click.on(EmmaUatLoanQuoter.BTN_CALCULATE_QUOTE));
        actor.attemptsTo(Click.on(EmmaUatLoanQuoter.ALERT_CALCULATE_QUOTE));

    }

    public static NewLoanAmount data(String amount,String addCpi) {

        return Tasks.instrumented(NewLoanAmount.class,amount,addCpi);
    }
}
