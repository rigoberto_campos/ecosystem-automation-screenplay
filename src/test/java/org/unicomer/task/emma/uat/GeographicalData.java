package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectList;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
import org.unicomer.utils.Wait;

public class GeographicalData implements Task {

    Customer customer;
    public GeographicalData(Customer customer){
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_PARISH,EmmaUatHome.DRPD_SELECT_PANEL, customer.getDepartment()));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_DISTRICT,EmmaUatHome.DRPD_SELECT_PANEL, customer.getDistrict()));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_RESIDENT_STATUS,EmmaUatHome.DRPD_SELECT_PANEL, customer.getResidentStatus()));
        actor.attemptsTo(Enter.theValue(customer.getMonthsHome()).into(EmmaUatHome.INPUT_MONTHS_CURRENT_ADDRESS));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_SEND_FORM));
        Wait.to(1);

    }

    public static GeographicalData data(Customer customer) {

        return Tasks.instrumented(GeographicalData.class, customer);
    }
}
