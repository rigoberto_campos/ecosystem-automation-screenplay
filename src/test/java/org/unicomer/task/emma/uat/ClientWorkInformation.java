package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectList;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;

public class ClientWorkInformation implements Task {

    Customer customer;
    public ClientWorkInformation(Customer customer){
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_OCCUPATION,EmmaUatHome.DRPD_SELECT_PANEL, customer.getOccupation()));
        actor.attemptsTo(Enter.theValue(customer.getBankendMonths()).into(EmmaUatHome.INPUT_WORK_BANKED_MONTHS));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_MAIN_ACTIVITY,EmmaUatHome.DRPD_SELECT_PANEL, customer.getJobMainActivity()));
        actor.attemptsTo(Enter.theValue(customer.getFullName()).into(EmmaUatHome.INPUT_EMPLOYER_NAME));
        actor.attemptsTo(Enter.theValue(customer.getAddress()).into(EmmaUatHome.TXT_CURRENT_WORK_ADDRESS));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaUatHome.INPUT_PHONE_WORK));
        actor.attemptsTo(Click.on(EmmaUatHome.DRP_WORK_START_DATE));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SELECT_YEARS));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_WORK_START_DATE,customer.getYearJ(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_WORK_START_DATE,customer.getMonthJ(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_WORK_START_DATE,customer.getDayJ(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaUatHome.DRP_POLITICAL_EXPOSED,EmmaUatHome.DRPD_SELECT_PANEL, "No "));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_SEND_FORM));
    }

    public static ClientWorkInformation data(Customer customer) {

        return Tasks.instrumented(ClientWorkInformation.class, customer);
    }
}
