package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectRadioButton;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

public class SearchDocumentTrn implements Task {

    String documentId;

    public SearchDocumentTrn(String documentId){
        this.documentId = documentId;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Enter.theValue(documentId).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT_CLIENT));
        actor.attemptsTo(Click.on(EmmaUatLoanQuoter.BTN_CHECK_CLIENT_LOAN));
    }
    public static SearchDocumentTrn data(String documentId) {

        return Tasks.instrumented(SearchDocumentTrn.class, documentId);
    }
}
