package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.unicomer.interactions.SelectList;
import org.unicomer.userInterfaces.emma.EmmaCreditApplicationStatus;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;

public class SelectTypeRequest implements Task {

    private String typeRequest;

    public SelectTypeRequest(String typeRequest){
        this.typeRequest = typeRequest;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.DRPD_TYPE_REQUEST));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,typeRequest, SelectList.getElement()));
    }

    public static SelectTypeRequest data(String typeRequest) {
        return Tasks.instrumented(SelectTypeRequest.class, typeRequest);
    }
}
