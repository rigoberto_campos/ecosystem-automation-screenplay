package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;

public class SearchDocumentId implements Task {

    String documentId;

    public SearchDocumentId(String documentId){
        this.documentId = documentId;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Enter.theValue(documentId).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT_CLIENT));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_CHECK_CLIENT));

    }

    public static SearchDocumentId data(String documentId) {

        return Tasks.instrumented(SearchDocumentId.class, documentId);
    }
}
