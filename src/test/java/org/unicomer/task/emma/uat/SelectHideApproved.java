package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.ScrollTo;
import org.unicomer.userInterfaces.emma.EmmaCreditApplicationStatus;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

import javax.swing.*;

public class SelectHideApproved implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.BTN_CHECK_OK));
        actor.attemptsTo(Click.on(EmmaCreditApplicationStatus.BTN_SHOW_APPROVED));
        actor.attemptsTo(Scroll.to(EmmaCreditApplicationStatus.MESSAGE_FOOTER));
    }

    public static SelectHideApproved data() {
        return Tasks.instrumented(SelectHideApproved.class);
    }
}
