package org.unicomer.task.emma.uat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

public class NewLoadQuoter implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaUatLoanQuoter.BTN_LOAD_QUOTER));
    }

    public static NewLoadQuoter data() {

        return Tasks.instrumented(NewLoadQuoter.class);
    }
}
