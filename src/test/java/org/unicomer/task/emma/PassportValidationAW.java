package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.userInterfaces.emma.EmmaUatHome;
import org.unicomer.utils.Wait;

public class PassportValidationAW implements Task {
    private static String userTrn;
    @Override
    public <T extends Actor> void performAs(T actor) {
        Wait.to(3);
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_NEW_CREDIT));
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_DRIVER_LICENCE));
        Wait.to(3);
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_NATIONAL_ID));
        Wait.to(3);
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUatHome.DRPD_PASSPORT));
        Wait.to(3);
        actor.attemptsTo(Enter.theValue(userTrn).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT_CLIENT));
        actor.attemptsTo(Click.on(EmmaUatHome.BTN_CHECK_CLIENT));
        Wait.to(3);

    }

    public static org.unicomer.task.emma.PassportValidationAW page(String user) {
        userTrn = user;
        return Tasks.instrumented(org.unicomer.task.emma.PassportValidationAW.class);
    }
}
