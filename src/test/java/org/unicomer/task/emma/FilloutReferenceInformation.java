package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaReferenceInformation;
import org.unicomer.interactions.SelectList;
import org.unicomer.utils.Wait;

public class FilloutReferenceInformation implements Task  {
    private final Customer customer;
    public FilloutReferenceInformation(Customer customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Enter.theValue(customer.getNameReference1()).into(EmmaReferenceInformation.INPUT_NAME_FIRST_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_RELATIONSHIP_FIRST_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.getRelationship1(), SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_TYPE_PHONE_FIST_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.getTypePhonereference1(), SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getNumberReference1()).into(EmmaReferenceInformation.INPUT_PHONE_FIRST_REFERENCE));
        actor.attemptsTo(Enter.theValue(customer.getNameReference2()).into(EmmaReferenceInformation.INPUT_NAME_SECOND_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_RELATIONSHIP_SECOND_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.getRelationship2(), SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_TYPE_PHONE_SECOND_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.getTypePhonereference1(), SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getNumberReference1()).into(EmmaReferenceInformation.INPUT_PHONE_SECOND_REFERENCE));
        actor.attemptsTo(Enter.theValue(customer.getNameReference1()).into(EmmaReferenceInformation.INPUT_NAME_THIRD_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_RELATIONSHIP_THIRD_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.getRelationship1(), SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_TYPE_PHONE_THIRD_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.getTypePhonereference1(), SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getNumberReference1()).into(EmmaReferenceInformation.INPUT_PHONE_THIRD_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.BTN_NEXT_DOCUMENTS));
        Wait.to(3);
    }
    public static FilloutReferenceInformation customer(Customer customer) {
        return Tasks.instrumented(FilloutReferenceInformation.class,customer);
    }
}
