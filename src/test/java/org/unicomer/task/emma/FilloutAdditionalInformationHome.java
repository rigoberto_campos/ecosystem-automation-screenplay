package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectList;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaAdditionalnformationHome;
import org.unicomer.utils.Wait;

public class FilloutAdditionalInformationHome implements Task  {
    private final Customer customer;
    public FilloutAdditionalInformationHome(Customer customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(customer.getStreetWork()).into(EmmaAdditionalnformationHome.INPUT_STREET_HOME));
        actor.attemptsTo(Enter.theValue(customer.getStreetWork()).into(EmmaAdditionalnformationHome.INPUT_REFERENCE_STREET));
        actor.attemptsTo(Click.on(EmmaAdditionalnformationHome.LIST_DATE_HOME));
        actor.attemptsTo(SelectList.since(EmmaAdditionalnformationHome.LIST_SELECT_DATE_HOME,"2020", SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaAdditionalnformationHome.LIST_SELECT_DATE_HOME,customer.getMonthJ(), SelectList.getDate()));
        actor.attemptsTo(Click.on(EmmaAdditionalnformationHome.LIST_TYPE_HOME));
        actor.attemptsTo(SelectList.since(EmmaAdditionalnformationHome.LIST_ELEMENTS_TYPE_HOME,customer.getResidentStatus(), SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaAdditionalnformationHome.BTN_NEXT_REFERENCES));
        Wait.to(3);
     }
    public static FilloutAdditionalInformationHome customer(Customer customer) {
        return Tasks.instrumented(FilloutAdditionalInformationHome.class,customer);
    }
}
