package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.interactions.SelectList;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaWorkInformation;
import org.unicomer.utils.*;

public class FilloutWorkInformation implements Task  {
    private final Customer customer;
    public FilloutWorkInformation(Customer customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(customer.getCompanyName()).into(EmmaWorkInformation.INPUT_NAME_COMPANY));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_DATE_COMPANY));
        actor.attemptsTo(Click.on(EmmaWorkInformation.BTN_SELECT_YEARS_COMPANY));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_SELECT_DATE_COMPANY,customer.getYearJ(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_SELECT_DATE_COMPANY,customer.getMonthJ(), SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_SELECT_DATE_COMPANY,customer.getDayJ(), SelectList.getDate()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_OCUPATION));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS_OCUPATION,customer.getOccupation(), SelectList.getOcupation()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_DEPARTMENT));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.getDepartment(), SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_DISTRICT));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.getDistrict(), SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_NEIGHBORHOOD));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.getNeighborhood(), SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getStreetWork()).into(EmmaWorkInformation.INPUT_STREET_WORK));
        actor.attemptsTo(Enter.theValue(customer.getStreetWork()).into(EmmaWorkInformation.INPUT_REFERENCE_STREET_WORK));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_TYPE_PHONE1));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.getPhoneTypeWork(), SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaWorkInformation.INPUT_PHONE_NUMBER1));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_TYPE_PHONE2));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.getPhoneTypeWork(), SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaWorkInformation.INPUT_PHONE_NUMBER2));
        actor.attemptsTo(Click.on(EmmaWorkInformation.BTN_NEXT_ADDITIONAL_HOME));
        Wait.to(3);
    }
    public static FilloutWorkInformation customer(Customer customer) {
        return Tasks.instrumented(FilloutWorkInformation.class,customer);
    }
}
