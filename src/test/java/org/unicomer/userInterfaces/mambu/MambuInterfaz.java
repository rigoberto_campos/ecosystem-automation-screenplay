package org.unicomer.userInterfaces.mambu;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.unicomer.constants.SystemConstants;

public class MambuInterfaz {

    public static final Target BTN_START = Target.the("BUTTON LOGIN").located(By.xpath("//a[@id='gwt-debug-federatedAuthenticationLink']"));
    public static final Target INPUT_EMAIL = Target.the("ENTER THE EMAIL").located(By.name("loginfmt"));
    public static final Target BTN_NEXT_AD = Target.the("Button Next").located(By.xpath("//input[contains(@type,'submit')]"));
    public static final Target BTN_NO_AD = Target.the("Button No").located(By.xpath("//input[contains(@type,'button')]"));
    public static final Target INPUT_RED_PASSWORD = Target.the("ENTER THE RED PASSWORD").located(By.xpath("//input[@name='passwd' and @aria-required='true']"));
    public static final Target INPUT_SEARCH = Target.the("ENTER TO SEARCH BOX").located(By.id("gwt-debug-searchInput"));
    public static final Target TR_CLIENT = Target.the("CLIENT BOX").located(By.xpath("(//table[@id='gwt-debug-resultsTable']//tr)[1]"));
    public static final Target LINK_AD = Target.the("LINK AD").located(By.id("gwt-debug-federatedAuthenticationLink"));
    public static final Target VALIDATE_ACCOUNT_STATUS = Target.the("VALIDATE ACCOUNT STATUS").located(By.xpath("//div[@id ='gwt-debug-number']"));

}
