package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaCreditApplicationStatus {

    public static final Target BTN_CREDIT_APPLICATION = Target.the("BUTTON CREDIT APPLICATION").located(By.xpath("(//span[contains(text(),'Credit application status')])[2]"));
    public static final Target DRPD_TYPE_REQUEST = Target.the("TYPE REQUEST CREDIT").located(By.id("requestType"));
    public static final Target BTN_CHECK_CLIENT_CREDIT = Target.the("BUTTON CHECK CLIENT CREDIT").located(By.xpath("//span[contains (text(),'CHECK')]"));
    public static final Target DRPD_TYPE_PROMOTION = Target.the("TYPE PROMOTION").located(By.id("mat-select-value-5"));
    public static final Target DRPD_TYPE_TERM_MONTHS = Target.the("TYPE TERM MONTHS").located(By.id("periodMonth"));
    public static final Target DRPD_TYPE_STORE = Target.the("TYPE STORE").located(By.xpath("//*[contains(@formcontrolname, 'store')][1]"));
    public static final Target CSR_NUMBER = Target.the("CSR NUMBER").located(By.xpath("//*[contains(@formcontrolname, 'storeNo')]"));
    public static final Target BTN_CHECK_OUT = Target.the("BUTTON CHECK OUT CREDIT APPLICATION").located(By.id("button-calculate-loan"));
    public static final Target ALERT_CREDIT_APPLICATION = Target.the("ALERT CREDIT APPLICATION").located(By.xpath("//mat-icon[contains(text(),'close')]"));
    public static final Target MESSAGE_REQUEST_ID = Target.the("MESSAGE REQUEST ID").located(By.xpath("//p[@class ='ng-star-inserted']"));
    public static final Target MESSAGE_CREDIT_POST_SALE = Target.the("MESSAGE CREDIT POST SALE").located(By.xpath("//*[@class='text-center']/p"));
    public static final Target BTN_CHECK_OK = Target.the("BUTTON CHECK OK CREDIT POST SALE").located(By.xpath("//*[@class='mat-dialog-actions']/button"));
    public static final Target BTN_SHOW_APPROVED = Target.the("BUTTON CHECK SHOW APPROVED").located(By.xpath("//p[@class = 'button-show mt-3']"));
    public static final Target MESSAGE_FOOTER = Target.the("MESSAGE FOOTER").located(By.xpath("//div[@class='footer']"));
}
