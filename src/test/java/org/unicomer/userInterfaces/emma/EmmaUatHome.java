package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaUatHome {

    public static final Target BTN_NEW_CREDIT = Target.the("BUTTON NEW CREDIT APPLICATION").located(By.xpath("(//span[contains(text(),'NEW CREDIT APPLICATION')])[2]"));
    public static final Target DRPD_DOCUMENT = Target.the("DROPDOWN CLIENT DOCUMENT TYPE").located(By.id("type"));
    public static final Target DRPD_DRIVER_LICENCE = Target.the("Select Driver Licence").located(By.xpath("//span[contains(text(),'Drivers Licence')]"));
    public static final Target DRPD_NATIONAL_ID = Target.the("Select National").located(By.xpath("//span[contains(text(),'National ID CARD')]"));
    public static final Target DRPD_PASSPORT = Target.the("Select National").located(By.xpath("//span[contains(text(),'Passport')]"));
    public static final Target INPUT_DOCUMENT = Target.the("INPUT CLIENT DOCUMENT").located(By.id("inputDoc"));
    public static final Target BTN_CHECK_CLIENT = Target.the("BUTTON TO CHECK IF CLIENT ALREADY EXIST").located(By.id("button-form-credit-validation"));
    public static final Target DRPD_SELECT_GENDER = Target.the("DROPDOWN TO SELECT GENDER").locatedBy("//*[@id='eb061ebb-e8c7-4aec-b23d-9bb4ae0bd2d1']");
    public static final Target DRPD_SELECT_PANEL = Target.the("PANEL TO SELECT AN OPPTION FROM DROPDOWN").locatedBy("//mat-option");
    public static final Target INPUT_COUNTRY = Target.the("INPUT TO ENTER COUNTRY OF CUSTOMER").locatedBy("//input[@aria-autocomplete='list']");
    public static final Target DRP_CNTRY_BIRD = Target.the("DROPDOWN TO SELECT COUNTRY OF BIRTH").locatedBy("//*[@id='e72496bb-79bb-4ae4-92af-457d6ae5b973']");
    public static final Target DRP_MARRIED_STATUS = Target.the("DROPDOWN TO SELECT MARRIED STATUS FROM THE CUSTOMER").locatedBy("//*[@id='d2f8a854-925f-4454-b3f4-58a9c1c4cf40']");
    public static final Target DRP_CNTRY_PANEL = Target.the("LIST OF COUNTRY").locatedBy("//*[@role='listbox']");
    public static final Target INPUT_NUMBER_DEPENDS = Target.the("iNPUTS TO ENTER THE NUMBER OF DEPENDS FROM THE CUSTOMER").locatedBy("//*[@name='d278a5eb-ba1e-4326-9354-b3f94794db62']");
    public static final Target DRP_PAYMENT_FRENQUENCY = Target.the("DROPDOWN TO SELECT THE PAYMENT FREQUENCY FROM THE CUSTOMER").locatedBy("//*[@id='accaed80-627c-4435-bd5e-6b7914b1fbea']");
    public static final Target INPUT_PAYMENT_INCOME = Target.the("INPUT TO ENTER THE PAYMENT INCOMING EVERY MONTH").locatedBy("//*[@name='ed6fb19c-cd98-4c6b-a754-52f3aa88c857']");
    public static final Target TXT_ADRRES = Target.the("TEXT AREA TO ENTER THE ADDRESS FROM CUSTOMER").locatedBy("//*[@name='c6d86e3e-ed10-41c1-8ac3-1bf5805651ac']");
    public static final Target BTN_SEND_FORM = Target.the("CONFIRM FORM AND GO TO THE NEXT STEPS").locatedBy("//*[@id='button-form-credit-qualification']");
    public static final Target BTN_CONFIRM_AMOUNT = Target.the("Confirm the incoming amount per month").locatedBy("//*[contains(text(),'CONFIRM INCOME')]");
    public static final Target DRP_OCCUPATION = Target.the("DROPDOWN TO SELECT THE MAIN OCCUPATION").locatedBy("//*[@id='e9df34d7-3b2a-4d31-9a4c-8d2b63d22e49']");
    public static final Target INPUT_WORK_BANKED_MONTHS = Target.the("INPUT FOR how many months have you been banked").locatedBy("//*[@name='e11d2c06-1215-41ce-98fe-c66959552c0b']");
    public static final Target DRP_MAIN_ACTIVITY = Target.the("INPUT FOR WORK MAIN ECONOMIC ACTIVITY").locatedBy("//*[@id='61855228-2ff8-46a8-a44a-d7e4b458c807']");
    public static final Target INPUT_EMPLOYER_NAME = Target.the("INPUT FOR EMPLOYER NAME").locatedBy("//*[@name='9d49b863-3585-4d07-9a11-58de90439c2d']");
    public static final Target TXT_CURRENT_WORK_ADDRESS = Target.the("IMPUT FOR YOUR CURRENT WORK ADDRESS").locatedBy("//*[@name='7389d35d-0175-47d9-a9dd-8c0b8c6ea5c6']");
    public static final Target INPUT_PHONE_WORK = Target.the("INPUT FOR PHONE WORK NUMBER").locatedBy("//*[@name='4281b5bd-cfb7-4daa-9890-9056cd129e5f']");
    public static final Target DRP_WORK_START_DATE = Target.the("LIST TO SELECT WORK START DATE").locatedBy("//*[@name='f9d7cc15-88a1-4472-bca7-2e3d2c20c355']");
    public static final Target DRP_PARISH = Target.the("DROPDOWN TO SELECT PARISH OF CUSTOMER").locatedBy("//*[@id='9b96dddd-b836-4b5b-b84c-02f11e8c6558']");
    public static final Target DRP_DISTRICT = Target.the("DROPDOWN TO SELECT DISTRICT OF CUSTOMER").locatedBy("//*[@id='26438c6e-da3c-4fe5-9b8a-556aa0df499f']");
    public static final Target DRP_RESIDENT_STATUS = Target.the("DROPDOWN TO SELECT RESIDENT STATUS OF CUSTOMER").locatedBy("//*[@id='a1c937a7-dab3-453c-a71b-8728d0bf4f97']");
    public static final Target INPUT_MONTHS_CURRENT_ADDRESS = Target.the("INPUT TO ENTER MONTHS AT CURRENT ADDRESS").locatedBy("//*[@name='225ad66d-4993-4969-b323-25f69ef1ed9a']");
    public static final Target INPUT_PERSONAL_REFERENCE_1 = Target.the("INPUT TO ENTER FIRST PERSONAL REFERENCE").locatedBy("//*[@name='8c6d2ace-2157-4c8e-8e7b-4726eb7c8bf2']");
    public static final Target INPUT_PHONE_REFERENCE_1 = Target.the("INPUT TO ENTER FIRST PERSONAL REFERENCE NUMBER").locatedBy("//*[@name='6a028cfe-04b9-41f3-8884-a02c5f2e9260']");
    public static final Target INPUT_PERSONAL_REFERENCE_2 = Target.the("INPUT TO ENTER SECOND PERSONAL REFERENCE").locatedBy("//*[@name='1acf74f0-268f-4fdd-96e9-3a0002f61043']");
    public static final Target INPUT_PHONE_REFERENCE_2 = Target.the("INPUT TO ENTER SECOND PHONE REFERENCE").locatedBy("//*[@name='213cb714-912b-45fd-8968-5d79a91cca09']");
    public static final Target DRP_POLITICAL_EXPOSED = Target.the("LIST TO SELECT IF CUSTOMER IS POLITICAL EXPOSED").locatedBy("//*[@id='15654e71-7d63-4402-8bf2-c80dc6698185']");
    public static final Target ICON_UPLOAD_DOCUMENT1 = Target.the("ICON BOX TO UPLOAD DOCUMENTS").locatedBy("(//*[contains(text(),'Front and back of customer ID')])");
    public static final Target ICON_UPLOAD_DOCUMENT2 = Target.the("ICON BOX TO UPLOAD DOCUMENTS").locatedBy("(//*[contains(text(),'Pay Slip')])");
    public static final Target ICON_UPLOAD_DOCUMENT3 = Target.the("ICON BOX TO UPLOAD DOCUMENTS").locatedBy("(//*[contains(text(),'Most recent Utility')])");
    public static final Target ICON_UPLOAD_DOCUMENT4 = Target.the("ICON BOX TO UPLOAD DOCUMENTS").locatedBy("(//*[contains(text(),'Job letter')])");
    public static final Target EMMA_WAIT_ELEMENT =  Target.the("WAIT ELEMENT").located(By.xpath("//*[contains(text(),'Loading...')]"));


}