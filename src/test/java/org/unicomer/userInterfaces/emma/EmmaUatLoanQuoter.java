package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import javax.lang.model.element.Element;

public class EmmaUatLoanQuoter {

    public static final Target BTN_LOAD_QUOTER = Target.the("BUTTON LOAD QUOTER").located(By.xpath("(//span[contains(text(),'Loan quoter')])[2]"));
    public static final Target BTN_CHECK_CLIENT_LOAN = Target.the("BUTTON TO CHECK LOAD QUOTER").located(By.xpath("//span[contains (text(),'CHECK CLIENT')]"));
    public static final Target INPUT_AMOUNT = Target.the("INPUT LOAN AMOUNT").located(By.id("amount"));
    public static final Target BTN_CALCULATE_QUOTE = Target.the("BUTTON CALCULATE QUOTER").located(By.xpath("//span[contains (text(),'CALCULATE QUOTE')]"));
    public static final Target ALERT_CALCULATE_QUOTE = Target.the("ALERT CALCULATE QUOTER").located(By.xpath("//div[@class ='ng-star-inserted']"));
    public static final Target MESSAGE_CALCULATE_QUOTE = Target.the("MESSAGE CALCULATE QUOTER").located(By.xpath("//p[@class='text-center']"));
//mat-icon[contains(text(),'close')]

    //p[@class ='ng-star-inserted']
}
