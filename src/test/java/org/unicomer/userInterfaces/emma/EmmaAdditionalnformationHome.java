package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaAdditionalnformationHome {
    public static final Target INPUT_STREET_HOME = Target.the("INPUT STREET HOME").located(By.xpath("//textarea[@name='c6d86e3e-ed10-41c1-8ac3-1bf5805651ac']"));
    public static final Target INPUT_REFERENCE_STREET = Target.the("INPUT REFERENCE STREET").located(By.xpath("//input[@name='f50af24d-18f5-4130-a5d1-1711f6f00cdf']"));
    public static final Target LIST_DATE_HOME = Target.the("LIST TYPE PHONE").located(By.xpath("//input[@name='225ad66d-4993-4969-b323-25f69ef1ed9a']"));
    public static final Target LIST_SELECT_DATE_HOME = Target.the("BUTTON SELECT YEAR").located(By.xpath("//div[contains(@class,'mat-calendar-body-cell-content mat-focus-indicator')]"));
    public static final Target LIST_TYPE_HOME = Target.the("LIST DEPARTMENT").located(By.id("a1c937a7-dab3-453c-a71b-8728d0bf4f97"));
    public static final Target LIST_ELEMENTS_TYPE_HOME = Target.the("LIST ELEMENTS").located(By.xpath("//span[ @class='mat-option-text']"));
    public static final Target BTN_NEXT_REFERENCES = Target.the("BUTTON CONTINUE REFERENCE").located(By.id("button-form-credit-qualification"));

}
