package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaWorkInformation {

    public static final Target INPUT_NAME_COMPANY = Target.the("INPUT NAME COMPANY").located(By.xpath("//input[@name='9d49b863-3585-4d07-9a11-58de90439c2d']"));
    public static final Target LIST_DATE_COMPANY = Target.the("LIST DATE COMPANY").located(By.xpath("//input[@name='f9d7cc15-88a1-4472-bca7-2e3d2c20c355']"));
    public static final Target BTN_SELECT_YEARS_COMPANY = Target.the("BUTTON SELECT YEARS COMPANY").located(By.xpath("//button[contains(@class,'mat-focus-indicator mat-calendar-period-button mat-button mat-button-base')]"));
    public static final Target LIST_SELECT_DATE_COMPANY = Target.the("BUTTON SELECT YEAR").located(By.xpath("//div[contains(@class,'mat-calendar-body-cell-content mat-focus-indicator')]"));
    public static final Target LIST_ELEMENTS = Target.the("LIST ELEMENTS").located(By.xpath("//span[ @class='mat-option-text']"));
    public static final Target LIST_OCUPATION = Target.the("LIST TYPE PHONE").located(By.xpath("//div[contains(text(),'Ocupaci' )]"));
    public static final Target LIST_ELEMENTS_OCUPATION = Target.the("LIST OCUPATION").located(By.xpath("//div[contains(@class,'ng-option ng-star-inserted')]"));
    public static final Target LIST_DEPARTMENT = Target.the("LIST DEPARTMENT").located(By.id("b013f3ab-c64e-4aaa-b9cf-5007ba8ae2bc"));
    public static final Target LIST_DISTRICT = Target.the("LIST DISTRICT").located(By.id("62000dfe-860d-41f3-874d-dd08337c9b23"));
    public static final Target LIST_NEIGHBORHOOD = Target.the("LIST NEIGHBORHOOD").located(By.id("35624e50-1438-46f1-9e75-5c0ef3c83cca"));
    public static final Target INPUT_STREET_WORK = Target.the("INPUT STREET WORK").located(By.xpath("//textarea[@name='7389d35d-0175-47d9-a9dd-8c0b8c6ea5c6']"));
    public static final Target INPUT_REFERENCE_STREET_WORK = Target.the("INPUT REFERENCE STREET WORK").located(By.xpath("//input[@name='a71301e7-a038-47ed-b7da-7bf23b231c3d']"));
    public static final Target LIST_TYPE_PHONE1 = Target.the("LIST TYPE PHONE").located(By.id("0a8af9d8-d511-4cc2-8191-3a61e5492127"));
    public static final Target INPUT_PHONE_NUMBER1 = Target.the("INPUT PHONE NUMBER").located(By.xpath("//input[@name='dc111361-4b84-4d69-8e1f-1e08c2380b35']"));
    public static final Target LIST_TYPE_PHONE2 = Target.the("LIST TYPE PHONE").located(By.id("c96c10c7-2974-407f-ab30-6f617723e10f"));
    public static final Target INPUT_PHONE_NUMBER2 = Target.the("INPUT PHONE NUMBER").located(By.xpath("//input[@name='028b2572-2300-4b43-9b8b-5dfbde0dabbe']"));
    public static final Target BTN_NEXT_ADDITIONAL_HOME = Target.the("BUTTON CONTINUE REFERENCE").located(By.id("button-form-credit-qualification"));

}
