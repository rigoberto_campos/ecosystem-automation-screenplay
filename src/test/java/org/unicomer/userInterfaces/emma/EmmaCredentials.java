package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaCredentials {
    public static final Target BTN_START = Target.the("BUTTON LOGIN").located(By.xpath("//div[contains(@class,'button-login text-center')]"));
    public static final Target INPUT_USERNAME = Target.the("ENTER THE USER").located(By.name("loginfmt"));
    public static final Target INPUT_PASSWORD = Target.the("ENTER THE PASSWORD").located(By.xpath("//input[@name='passwd' and @aria-required='true']"));
    public static final Target BTN_SEND_CREDENTIAL = Target.the("BUTTON SUBMIT").located(By.id("idSIButton9"));
    public static final Target BTN_SESSION_ACTIVE = Target.the("BUTTON NOT ACTIVE").located(By.id("idBtn_Back"));

}
