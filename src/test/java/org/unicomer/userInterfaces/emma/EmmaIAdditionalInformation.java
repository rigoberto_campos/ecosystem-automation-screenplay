package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaIAdditionalInformation {
    public static final Target LIST_COUNTRY_BIRTH = Target.the("LIST COUNTRY BIRTH").located(By.xpath("//div[contains(text(),'Seleccione')]"));
    public static final Target LIST_ELEMENTS_COUNTRY_BIRTH = Target.the("LIST ELEMTNS COUNTRY BIRTH").located(By.xpath("//span[@class='ng-option-label ng-star-inserted']"));
    public static final Target INPUT_DEPENDENTS = Target.the("INPUT DEPENDENTS").located(By.xpath("//input[@name='d278a5eb-ba1e-4326-9354-b3f94794db62']"));
    public static final Target LIST_TYPE_PHONE = Target.the("LIST TYPE PHONE").located(By.id("a4ededf5-7008-4a3a-871c-83f868a2abb6"));
    public static final Target LIST_ELEMENTS_TYPE_PHONE = Target.the("LIST ELEMENTS TYPE PHONE").located(By.xpath("//span[ @class='mat-option-text']"));
    public static final Target INPUT_PHONE_NUMBER = Target.the("INPUT PHONE NUMBER").located(By.xpath("//input[@name='0ea1447c-b13e-476d-86c4-12ee9db1961c']"));
    public static final Target BTN_NEXT = Target.the("BUTTON CONTINUE ADDITIONAL INFORMATION").located(By.id("button-form-credit-qualification"));

}
