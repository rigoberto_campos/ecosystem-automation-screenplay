package org.unicomer.userInterfaces.workflow;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
public class WorkflowRequestTracking {
    public static final Target INPUT_TRN = Target.the("Input TRN").located(By.xpath("//input[@test-id='searchWorking-txt']"));
    public static final Target BTN_SEARCH_TRN = Target.the("Button Search TRN").located(By.xpath("//button[@test-id='searchWorking-Btn']"));
    public static final Target BTN_CLICK_REQUEST = Target.the("Button Click Request").located(By.xpath("//button[@test-id='requestNumber-Btn']"));
    public static final Target BTN_FINISH_VERIFICATION = Target.the("Button Finish Verification").located(By.xpath("//button[@test-id='saveVerified-btn'][2]"));
    public static final Target BTN_SAVE_DATA = Target.the("Button Save Data").located(By.xpath("//button[@test-id='saveVerified-btn']"));
    public static final Target BTN_SAVE_DATA_JM = Target.the("Button Save Data").located(By.xpath("//button[@test-id='saveVerified-btn']"));
    public static final Target BTN_CONFIRM_DATA = Target.the("Button Confirm Data").located(By.xpath("//button[@test-id='confirm-btn']"));
    public static final Target BTN_CANCEL_DATA = Target.the("Button Cancel Data").located(By.xpath("//button[@test-id='cancel-btn']"));
    //Lista
    public static final Target LIST_GENERAL_INFO = Target.the("List General Info").located(By.xpath("//*[@test-id='menu-dwpn']"));

    public static final Target OPTION_MENU = Target.the("USE FOR OPTION SUB MENU").locatedBy("//mat-option//span");
    public static final Target SELECT_RESOLVE_IDENTIFICATIONS = Target.the("Select Resolve").located(By.xpath("//span[contains(text(),'Identifications')]"));
    public static final Target SELECT_RESOLVE_ADDRESSES = Target.the("Select Resolve").located(By.xpath("//span[contains(text(),'Addresses')]"));
    public static final Target SELECT_RESOLVE_PHONES = Target.the("Select Resolve").located(By.xpath("//span[contains(text(),'Phones')]"));
    public static final Target SELECT_RESOLVE_FINANCIAL_INFORMATION = Target.the("Select Resolve").located(By.xpath("//span[contains(text(),'Financial information')]"));
    public static final Target SELECT_RESOLVE_REFERENCES = Target.the("Select Resolve").located(By.xpath("//span[contains(text(),'References')]"));
    public static final Target SELECT_RESOLVE = Target.the("Select Resolve").located(By.xpath("//span[contains(text(),'Resolve')]"));
    //Lista
    public static final Target BTN_STATUS = Target.the("Button Click Status").located(By.xpath("//mat-select[@test-id='newStatus-dpw']"));
    public static final Target BTN_DATA_VERIFICATION = Target.the("Button Data Verification").located(By.xpath("//span[contains(text(),' Data verification ')]"));
    public static final Target BTN_SUB_STATUS = Target.the("Button sub status").located(By.xpath("//*[@test-id='subStatus-dpw']"));
    public static final Target BTN_DATA_VALIDATION = Target.the("Button Data Validation").located(By.xpath("//span[contains(text(),' Data validation process ')]"));
    public static final Target BTN_SELECT_APPROVED = Target.the("Button Select Approved").located(By.xpath("//span[contains(text(),' Approved ')]"));
    public static final Target BTN_SAVE_CHANGE = Target.the("Button Save Change").located(By.xpath("//button[@test-id='saveNewStatus-btn']"));
    public static final Target BTN_SELECT_ALL_REQUEST = Target.the("Button Select All Request").located(By.xpath("//a[@test-id='applications-Opt']"));
    public static final Target INPUT_SEARCH_DOCUMENT_REQUEST = Target.the("Input Search Document Request").located(By.xpath("//a[@test-id='applications-Opt']"));
    public static final Target BTN_SEARCH = Target.the("Button Search Document Request").located(By.xpath("//button[@test-id='searchApplications-Btn']"));
    public static final Target BTN_REQUEST_NUMBER = Target.the("Button Request Number").located(By.xpath("//*[@test-id='spinner-component']"));
}
