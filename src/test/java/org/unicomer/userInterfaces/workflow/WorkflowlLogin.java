package org.unicomer.userInterfaces.workflow;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class WorkflowlLogin
{
    public static final Target INPUT_USERNAME = Target.the("ENTER THE USERNAME").located(By.name("username"));
    public static final Target INPUT_PASSWORD = Target.the("ENTER THE PASSWORD").located(By.name("password"));
    public static final Target BTN_CREDENTIALS = Target.the("SUBMIT CREDENTIALS").located(By.id("kc-login"));
}
