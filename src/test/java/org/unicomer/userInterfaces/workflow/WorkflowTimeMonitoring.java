package org.unicomer.userInterfaces.workflow;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.unicomer.constants.SystemConstants;
import org.unicomer.utils.ConstantReader;

public class WorkflowTimeMonitoring {
    public String user =  ConstantReader.getInstance().getProperties(SystemConstants.WORKF_USERNAME);
    public static final Target BTN_SEARCH_BY_DOCUMENT = Target.the("Button Search by Document").located(By.xpath("/html/body/app-root/app-main-layout/mat-sidenav-container/mat-sidenav-content/app-time-monitoring/div[2]/div[3]/mat-form-field[1]/div/div[1]/div[3]"));
    public static final Target BTN_SELECT_DOCUMENT = Target.the("Button Select Document").located(By.xpath("//span[contains(text(),'Document')]"));
    public static final Target INPUT_TRN = Target.the("Input TRN").located(By.xpath("//input[@test-id='search-txt']"));
    public static final Target BTN_SEARCH_TRN = Target.the("Button Search TRN").located(By.xpath("//button[@test-id='search-btn']"));
    public static final Target BTN_SELECT_REQUEST = Target.the("Button Select Request").located(By.xpath("(//*[@test-id='reassign-all-checkbox'])[2]"));
    public static final Target BTN_ASSIGN_REQUEST = Target.the("Button Assign Request").located(By.xpath("//button[@test-id='reassign-btn']"));
    public static final Target BTN_SELECT_USER = Target.the("Button Select User").located(By.xpath("//input[@value='credit.analyst@unicomer.com']"));
    public static final Target BTN_CONFIRM = Target.the("Button Confirm").located(By.xpath("//button[@test-id='modal-action-btn']"));

}
