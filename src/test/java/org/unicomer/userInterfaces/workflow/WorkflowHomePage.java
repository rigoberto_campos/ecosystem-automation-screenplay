package org.unicomer.userInterfaces.workflow;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class WorkflowHomePage {
    public static final Target LIST_COUNTRY = Target.the("List Country").located(By.id("mat-select-0"));
    public static final Target LIST_ELEMENTS = Target.the("LIST").located(By.xpath("//div[@role='listbox']"));
    public static final Target LIST_LANGUAGE = Target.the("List Language").located(By.id("mat-select-2"));
    public static final Target SPINNER = Target.the("Spinner Componnent").located(By.xpath("//*[@test-id='spinner-component']"));
}
