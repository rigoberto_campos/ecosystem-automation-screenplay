package org.unicomer;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/emma/jm/new_cash_loan.feature",
        tags = "@PostSaleEvaluation"
)
public class UatNewCashLoan {
}
