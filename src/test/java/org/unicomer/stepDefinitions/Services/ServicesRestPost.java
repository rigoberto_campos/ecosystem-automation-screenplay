package org.unicomer.stepDefinitions.Services;

import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.task.services.cashLoan.GetToken;
import org.unicomer.task.services.cashLoan.GetTokenTest2;
import org.unicomer.task.services.cashLoan.PartThree;
import org.unicomer.task.services.cashLoan.PartTwo;

public class ServicesRestPost {
    private Actor admin = Actor.named("admin");

    /*
    @Before
    public void setup() {
        setTheStage(new OnlineCast());
    }

    @Given("the user test Post {string}")
    public void the_user_test_post(String url) {
        admin.whoCan(CallAnApi.at(url));
    }

    @When("use the {string}")
    public void use_the(String endpoint, DataTable infoCustomer) {
        admin.attemptsTo(ExecutionPostToken.createUser(endpoint,infoCustomer));
    }

    @Then("validate the statusPost {int} and {string}")
    public void validate_the_status_post_and(Integer statusCode, String name) {
        admin.should(seeThatResponse(response -> response.statusCode(statusCode).body("name", equalTo(name))));
    }

     */

    @When("Disbursement Cosacs")
    public void use_the() {
        admin.attemptsTo(GetTokenTest2.getInformation());
        admin.attemptsTo(GetToken.getInformation());
        admin.attemptsTo(PartTwo.getInformation());
        admin.attemptsTo(PartThree.getInformation());
    }
}

