package org.unicomer.stepDefinitions.mambu;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import org.hamcrest.Matchers;
import org.unicomer.questions.ValidateCreditApproved;
import org.unicomer.task.mambu.uat.ConsultDocument;
import org.unicomer.task.mambu.uat.NavigateSubMenu;
import org.unicomer.utils.UtilsManager;

public class ConsultClient {
    private Actor admin = UtilsManager.getInstance().getActor();

    @When("consult the {string} exist values")
    public void consultDocumentExist(String documentId ){
        admin.attemptsTo(ConsultDocument.insert(documentId));
    }

    @When("select the {string} of consult")
    public void selectOptionSubMenu(String option){
        admin.attemptsTo(NavigateSubMenu.data(option));
    }

    @Then("validate of the application with {string}")
    public void validate_of_the_application(String message) {
        admin.should(GivenWhenThen.seeThat(ValidateCreditApproved.is(), Matchers.containsString(message)));
    }
}
