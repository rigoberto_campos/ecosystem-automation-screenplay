package org.unicomer.stepDefinitions.emma;

import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.models.Customer;
import org.unicomer.task.emma.uat.SearchDocumentId;
import org.unicomer.task.emma.uat.SelectDocumentType;
import org.unicomer.utils.UtilsManager;

public class DocumentValidationUAT {

    private Customer customer = UtilsManager.getInstance().getCustomer();
    private Actor admin = UtilsManager.getInstance().getActor();

    @When("validate the document ID dose not exist")
    public void validateDocumentId(){
        admin.attemptsTo(SelectDocumentType.data(customer.getTypeId()));
        admin.attemptsTo(SearchDocumentId.data(customer.getDocumentId()));
    }

}
