package org.unicomer.stepDefinitions.emma;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.models.Customer;
import org.unicomer.questions.ValidateMessageLoanQuoter;
import org.unicomer.task.emma.uat.*;
import net.serenitybdd.screenplay.GivenWhenThen;
import org.hamcrest.Matchers;
import org.unicomer.utils.UtilsManager;

public class LoanQuater {

    private Customer customer = UtilsManager.getInstance().getCustomer();
    private Actor admin = UtilsManager.getInstance().getActor();

    @When("the user click on loan quater menu")
    public void createLoadQuoter(){
        admin.attemptsTo(NewLoadQuoter.data());
    }

    @When("validate the document TRN and insert loans values")
    public void validateDocumentTrn(){
        admin.attemptsTo(SearchDocumentTrn.data(customer.getDocumentId()));
        admin.attemptsTo(NewLoanAmount.data(customer.getLoanAmount(),customer.getAddCpi()));

    }

    @Then("validate the loan quoter of the application {string}")
    public void validate_the_loan_quoter_of_the_application(String message) {
        admin.should(GivenWhenThen.seeThat(ValidateMessageLoanQuoter.is(), Matchers.containsString(message)));
    }

}


