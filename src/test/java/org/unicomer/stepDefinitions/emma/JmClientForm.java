package org.unicomer.stepDefinitions.emma;

import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.models.Customer;
import org.unicomer.task.emma.uat.*;
import org.unicomer.utils.UtilsManager;


public class JmClientForm {
    private Customer customer = UtilsManager.getInstance().getCustomer();
    private Actor admin = UtilsManager.getInstance().getActor();

    @When("admin user try to create new client")
    public void createNewClient(){
        admin.attemptsTo(NewClientForm.data());
    }

    @When("admin user enter basic information from the client")
    public void basicInfo(){
        admin.attemptsTo(ClientBasicInfo.data(customer));
        admin.attemptsTo(ClientPersonalInformation.data(customer));
        admin.attemptsTo(ClientWorkInformation.data(customer));
        admin.attemptsTo(GeographicalData.data(customer));
        admin.attemptsTo(ContactInformation.data(customer));
        admin.attemptsTo(UploadDocument.data(customer));
    }

}
