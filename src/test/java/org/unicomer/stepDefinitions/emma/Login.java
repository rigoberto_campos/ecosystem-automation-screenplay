package org.unicomer.stepDefinitions.emma;

import io.cucumber.java.en.Given;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.task.emma.Navigate;
import org.unicomer.task.emma.SelectCountry;
import org.unicomer.utils.UtilsManager;

public class Login {

    private Actor admin = UtilsManager.getInstance().getActor();

    @Given("the user into uat EMMA {string} with the credentials")
    public void the_user_into_emma_with_the_credentials(String emma) {
        admin.attemptsTo(Navigate.page(emma));
    }

    @Given("select the country uat emma")
    public void select_the_country() {
        admin.attemptsTo(SelectCountry.the());
    }

}
