package org.unicomer.stepDefinitions.emma;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import org.hamcrest.Matchers;
import org.unicomer.models.Customer;
import org.unicomer.models.Request;
import org.unicomer.questions.ValidateMessageCreditApplication;
import org.unicomer.questions.ValidateMessageCreditPostSale;
import org.unicomer.task.emma.uat.*;
import org.unicomer.utils.UtilsManager;

public class CreditApplicationStatus {

    private Customer customer = UtilsManager.getInstance().getCustomer();
    private Actor admin = UtilsManager.getInstance().getActor();
    private Request request = UtilsManager.getInstance().getRequest();

    @When("the user enter basic information from the request")
    public void basicInfoRequest(){
        admin.attemptsTo(SelectAvailablePromotion.data(request.getSelectPromotion(),request.getTermMonths(),request.getStore(),request.getCsrNumber()));

    }

    @Then("validate the credit request of the application {string}")
    public void validate_the_credit_request_of_the_application(String message) {
        admin.should(GivenWhenThen.seeThat(ValidateMessageCreditApplication.is(), Matchers.containsString(message)));
    }

    @When("validate the documentId TRN and insert credit values")
    public void validateDocumentTrnCredit(){
        admin.attemptsTo(SelectDocumentType.data(customer.getTypeId()));
        admin.attemptsTo(SearchDocumentTrnCredit.data(customer.getDocumentId(),request.getTypeRequest()));
    }

    @Then("validate the credit post sale evaluation of the application {string}")
    public void validate_the_credit_post_sale_evaluation_of_the_application(String message) {
        admin.should(GivenWhenThen.seeThat(ValidateMessageCreditPostSale.is(), Matchers.containsString(message)));
    }

    @Then("validate uploaded documents")
    public void validate_uploaded_documents() {
        admin.attemptsTo(SelectHideApproved.data());
    }


}
