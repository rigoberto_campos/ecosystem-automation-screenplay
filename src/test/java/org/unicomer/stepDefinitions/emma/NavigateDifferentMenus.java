package org.unicomer.stepDefinitions.emma;

import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.models.Customer;
import org.unicomer.models.Request;
import org.unicomer.task.emma.uat.NavigateMenu;
import org.unicomer.utils.UtilsManager;

public class NavigateDifferentMenus {

    private Customer customer = UtilsManager.getInstance().getCustomer();
    private Actor admin = UtilsManager.getInstance().getActor();
    private Request request = UtilsManager.getInstance().getRequest();

    @When("the user click on {string} menu")
    public void createCreditApplication(String menu){
        admin.attemptsTo(NavigateMenu.data(menu));
    }

}