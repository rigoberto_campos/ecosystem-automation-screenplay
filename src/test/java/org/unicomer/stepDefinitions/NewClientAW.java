package org.unicomer.stepDefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.chrome.ChromeDriver;
import org.unicomer.task.emma.PassportValidationAW;
import org.unicomer.utils.DriverManager;

public class NewClientAW {
    private ChromeDriver hisBrowser = DriverManager.getInstance().getChromeDriver();
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        admin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Then("validate different type of document {string}")
    public void validateDifferentTypeOfDocument(String pass) {
        admin.attemptsTo(PassportValidationAW.page(pass));
    }
}
