package org.unicomer.stepDefinitions;

import io.cucumber.java.en.Given;
import org.unicomer.models.Customer;
import org.unicomer.models.Request;
import org.unicomer.utils.UtilsManager;

public class CreateCustomerModel {
    @Given("DataTable customer data")
    public void customer_data(Customer customer) {
        UtilsManager.getInstance().setCustomer(customer);
    }

    @Given("DataTable request data")
    public void request_data(Request request) {
        UtilsManager.getInstance().setRequest(request);
    }

}
