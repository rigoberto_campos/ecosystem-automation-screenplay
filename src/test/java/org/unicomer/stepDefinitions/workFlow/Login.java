package org.unicomer.stepDefinitions.workFlow;

import io.cucumber.java.en.Given;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.task.workflow.Navigate;
import org.unicomer.task.workflow.SelectCountry;
import org.unicomer.utils.UtilsManager;

public class Login {


    private Actor admin = UtilsManager.getInstance().getActor();

    @Given("User navigate into Workflow {string} with the credentials")
    public void navigate_into_WorkFlow(String workflowUrl) {
        admin.attemptsTo(Navigate.page(workflowUrl));
    }
    @Given("Select the country")
    public void select_country_Workflow(){admin.attemptsTo(SelectCountry.the());}

}
