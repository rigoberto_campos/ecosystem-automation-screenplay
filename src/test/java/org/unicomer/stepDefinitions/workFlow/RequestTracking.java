package org.unicomer.stepDefinitions.workFlow;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.task.workflow.*;
import org.unicomer.utils.UtilsManager;

public class RequestTracking {
    private Actor admin = UtilsManager.getInstance().getActor();

    @When("Approved client request with document {string}")
    public void searchRequest (String trn){
        admin.attemptsTo(SearchClientRequestTrn.page(trn));
        admin.attemptsTo(DataValidationClient.page(trn));
        admin.attemptsTo(ConfirmValidationClient.page());
    }

    @When("Approved loan request with document {string}")
    public void loanRequest (String trn){
        admin.attemptsTo(SearchClientRequestTrn.page(trn));
        admin.attemptsTo(ConfirmValidationClient.page());
    }
}
