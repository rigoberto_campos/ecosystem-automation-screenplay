package org.unicomer.stepDefinitions.workFlow;

import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.utils.UtilsManager;

public class SelectMenu {
    private Actor admin = UtilsManager.getInstance().getActor();

    @Then("Select {string} menu in Workflow")
    public void select_menu_workflow(String menu){
        admin.attemptsTo(org.unicomer.task.workflow.SelectMenu.page(menu));
    }
}
