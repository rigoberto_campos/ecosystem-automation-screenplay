package org.unicomer.stepDefinitions.workFlow;

import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.Actor;
import org.unicomer.task.workflow.*;
import org.unicomer.utils.UtilsManager;

public class TimeMonitoring {
    private Actor admin = UtilsManager.getInstance().getActor();

    @Then("Assign request in workflow with client document {string}")
    public void searchTimeMonitoring(String trn){
        admin.attemptsTo(TimeMonitoringAssign.page(trn));
    }
}
