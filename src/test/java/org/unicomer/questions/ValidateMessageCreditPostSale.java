package org.unicomer.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.unicomer.userInterfaces.emma.EmmaCreditApplicationStatus;

public class ValidateMessageCreditPostSale implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(EmmaCreditApplicationStatus.MESSAGE_CREDIT_POST_SALE).asString().answeredBy(actor);
    }
    public static ValidateMessageCreditPostSale is() {
        return new ValidateMessageCreditPostSale();
    }
}