package org.unicomer.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.unicomer.userInterfaces.mambu.MambuInterfaz;

public class ValidateCreditApproved implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(MambuInterfaz.VALIDATE_ACCOUNT_STATUS).asString().answeredBy(actor);
    }
    public static ValidateCreditApproved is() {
        return new ValidateCreditApproved();
    }
}
