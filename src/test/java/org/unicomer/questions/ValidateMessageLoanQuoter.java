package org.unicomer.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

public class ValidateMessageLoanQuoter implements Question<String>{

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(EmmaUatLoanQuoter.MESSAGE_CALCULATE_QUOTE).asString().answeredBy(actor);
    }
    public static ValidateMessageLoanQuoter is() {
        return new ValidateMessageLoanQuoter();
    }
}
