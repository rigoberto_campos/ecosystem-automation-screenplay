package org.unicomer.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.CurrentVisibility;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.unicomer.utils.DriverManager;
import org.unicomer.utils.Wait;

import java.time.Duration;

public class LoadingElement implements Question<Boolean> {

    int count;
    private Target waitElement;

    public LoadingElement(Target waitElement){
        this.waitElement = waitElement;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        count = 0;
        DriverManager.getInstance().getChromeDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        Boolean isVisible = visible(actor);
        DriverManager.getInstance().getChromeDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(90));
        return isVisible;
    }

    private Boolean visible(Actor actor){
        Wait.to(1);
        try {
            Boolean visible = CurrentVisibility.of(waitElement).answeredBy(actor);
            if(visible){
                count = count + 1;
                System.out.println(count);
                if(count > 120){
                    return false;
                }
                return visible(actor);
            }
            return false;
        }catch (StaleElementReferenceException | NoSuchElementException e){
            return false;
        }
    }

    public static LoadingElement isVisible(Target element) {
        return new LoadingElement(element);
    }
}
