package org.unicomer.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.unicomer.userInterfaces.emma.EmmaCreditApplicationStatus;
import org.unicomer.userInterfaces.emma.EmmaUatLoanQuoter;

public class ValidateMessageCreditApplication implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(EmmaCreditApplicationStatus.MESSAGE_REQUEST_ID).asString().answeredBy(actor);
    }
    public static ValidateMessageCreditApplication is() {
        return new ValidateMessageCreditApplication();
    }
}
