package org.unicomer;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/emma/py/new_client.feature",
        tags = "@CreditoConsumo"
)
public class PyE2ENewClient {

}
