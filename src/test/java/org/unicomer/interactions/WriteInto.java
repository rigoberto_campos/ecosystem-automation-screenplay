package org.unicomer.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;

public class WriteInto implements Interaction {

    static String customerValue;
    Target input;

    int count = 0;

    public void fillInput(Actor actor){
        actor.attemptsTo(Enter.theValue(customerValue).into(input));
        if(count > 60){return;}
        if(input.resolveFor(actor).getValue() == ""){
            count=count +1;
            fillInput(actor);
        }
    }

    public WriteInto(Target input){
        this.input = input;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        count = 0;
        fillInput(actor);
    }

    public static WriteInto theInput(Target input,String value) {
        customerValue = value;
        return Tasks.instrumented(WriteInto.class, input);
    }
}
