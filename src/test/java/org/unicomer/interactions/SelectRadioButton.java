package org.unicomer.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SelectRadioButton implements Interaction {
    private static String txtRadioButton;
    Target OPTION_CPI = Target.the("OPTION CPI").located(By.xpath("//span[contains (text(),'"+ txtRadioButton +"')]"));

    public SelectRadioButton(String textoRadioButton) {
        this.txtRadioButton = textoRadioButton;
    }
    @Override

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(OPTION_CPI));
    }

    public static SelectRadioButton with(String txtRadioButton){
        SelectRadioButton.txtRadioButton = txtRadioButton;
        return Tasks.instrumented(SelectRadioButton.class,txtRadioButton);
    }

}
