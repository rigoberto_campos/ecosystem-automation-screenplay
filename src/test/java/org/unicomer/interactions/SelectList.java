package org.unicomer.interactions;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebElement;
import net.serenitybdd.screenplay.targets.Target;
import java.util.List;

public class SelectList implements Interaction {
    private static final String ELEMENTS = "//span[@class='mat-option-text']";
    private static final String ELEMENTS_DATE = "//div[contains(@class,'mat-calendar-body-cell-content mat-focus-indicator')]";
    private static final String ELEMENTS_COUNTRY = "//span[@class='ng-option-label ng-star-inserted']";
    private static final String ELEMENTS_OCUPATION = "//div[contains(@class,'ng-option ng-star-inserted')]";
    private Target list;
    private String opcion;
    private String typeElement;
    private Target labelList;
    private String optionTobe;

    public SelectList(Target list, String opcion, String typeElement) {
        this.list=list;
        this.opcion=opcion;
        this.typeElement=typeElement;
    }

    public SelectList(Target list, Target labelList, String optionTobe) {
        this.list=list;
        this.labelList=labelList;
        this.optionTobe=optionTobe;
    }

    private void selectOption(Actor actor){
        try {
            actor.attemptsTo(Click.on(list).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
            labelList.resolveFor(actor).findElement(By.xpath(getPathOpt())).click();
        }catch (ElementClickInterceptedException e){
            actor.attemptsTo(JavaScriptClick.on(list));
            labelList.resolveFor(actor).findElement(By.xpath(getPathOpt())).click();
        }
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if(!(optionTobe==null || optionTobe=="")){
            selectOption(actor);
            return;
        }

        List<WebElement> listObject = list.resolveFor(actor).findElements(By.xpath(typeElement));
        for ( int i=0; i<listObject.size(); i++){
            if (listObject.get(i).getText().equals(opcion)){
                listObject.get(i).click();
                break;
            }
        }
    }

    public static String getElement(){
        return ELEMENTS;
    }

    public static String getDate(){
        return ELEMENTS_DATE;
    }

    public static String getCountry(){
        return ELEMENTS_COUNTRY;
    }

    public static String getOcupation(){
        return ELEMENTS_OCUPATION;
    }

    private String getPathOpt(){
        return "//*[contains(text(), '"+optionTobe+"')]";
    }

    public static SelectList since(Target list, String opcion, String typeElement){
        return new SelectList(list,opcion,typeElement);
    }

    public static SelectList since(Target list, Target label, String opcion){
        return new SelectList(list,label,opcion);
    }

}
