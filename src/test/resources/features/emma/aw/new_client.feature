@EmmaAw
Feature: Emma New client JM uat

  @NewClient
  Scenario Outline: New credit application Emma Aw uat
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    Then validate different type of document "<passport>"
    Examples:
      | country | passport |
      | Aruba   | K199181  |
