@Paraguay
Feature: New client

  @CreditoConsumo
  Scenario Outline: New credit application Paraguay
    Given DataTable customer data
      | typeId   | documentId   | creditLine   | firstName   | secondName   | firstLastName   | secondLastName   | gender   | civilStatus   | birthDate   | numberMovil   | email   | sourceIncome   | frecuencyPaid   | income   | department   | district   | neighborhood   | country   | dependsNumber   | phoneType   | numberPhoneAdditional   | docCreationDate   | docFinalDate   | companyName   | employStartDate   | occupation   | streetWork   | phoneTypeWork   | streetHome   | residentStatus   | nameReference1   | relationship1   | typePhonereference1   | numberReference1   | nameReference2   | relationship2   | typePhonereference2   | numberReference2   |
      | <typeId> | <documentId> | <creditLine> | <firstName> | <secondName> | <firstLastName> | <secondLastName> | <gender> | <civilStatus> | <birthDate> | <numberMovil> | <email> | <sourceIncome> | <frecuencyPaid> | <income> | <department> | <district> | <neighborhood> | <country> | <dependsNumber> | <phoneType> | <numberPhoneAdditional> | <docCreationDate> | <docFinalDate> | <companyName> | <employStartDate> | <occupation> | <streetWork> | <phoneTypeWork> | <streetWork> | <residentStatus> | <nameReference1> | <relationship1> | <typePhonereference1> | <numberReference1> | <nameReference2> | <relationship2> | <typePhonereference2> | <numberReference2> |
    Given the user into EMMA "<country>" with the credentials
    And select the country
    When fill out customer data
    And fill out additional information of the customer
    And fill out work information
    And fill out additional home information
    And fill out information about references
    And upload the documents for the "<creditLine>"
    Then validate the creation of the application "<message>"
    #Es necesario cambiar los datos del ejemplo para que pueda crear al cliente.
    Examples:
      | country  | typeId              | documentId | creditLine                  | firstName | secondName | firstLastName | secondLastName | gender    | civilStatus | birthDate  | numberMovil | email             | sourceIncome        | frecuencyPaid | income   | department | district | neighborhood | dependsNumber | phoneType      | numberPhoneAdditional | companyName | employStartDate | occupation      | streetWork | phoneTypeWork          | residentStatus | nameReference1 | relationship1 | typePhonereference1       | numberReference1 | nameReference2 | relationship2 | typePhonereference2       | numberReference2 | message                                                 | docCreationDate | docFinalDate |
      | Paraguay | Cédula de Identidad | 1199219    | Línea de crédito de consumo | David     | Jhon       | Campos        | Vega           | Masculino | Soltero     | 2000-MAY-7 | 0982326326  | prueba8@gmail.com | Dependiente con IPS | Mensual       | 15000000 | ASUNCION   | ASUNCION | SAJONIA      | 0             | Teléfono móvil | 0981326326            | Unicomer    | 2020-MAY-7      | Administrador/a | Calle 5    | Teléfono móvil trabajo | Propia         | Madre Cliente  | Madre         | Teléfono móvil referencia | 0984563623       | Padre Cliente  | Padre         | Teléfono móvil referencia | 0981236536       | ¡La aplicación al crédito ha sido completada con éxito! | 2020-MAY-7      | 2026-MAY-7   |

  @MicroCredito
  Scenario Outline: New credit application Paraguay
    Given DataTable customer data
      | typeId   | documentId   | creditLine   | firstName   | secondName   | firstLastName   | secondLastName   | gender   | civilStatus   | birthDate   | numberMovil   | email   | sourceIncome   | frecuencyPaid   | income   | department   | district   | neighborhood   | country   | dependsNumber   | phoneType   | numberPhoneAdditional   | docCreationDate   | docFinalDate   | companyName   | employStartDate   | occupation   | streetWork   | phoneTypeWork   | streetHome   | residentStatus   | nameReference1   | relationship1   | typePhonereference1   | numberReference1   | nameReference2   | relationship2   | typePhonereference2   | numberReference2   |
      | <typeId> | <documentId> | <creditLine> | <firstName> | <secondName> | <firstLastName> | <secondLastName> | <gender> | <civilStatus> | <birthDate> | <numberMovil> | <email> | <sourceIncome> | <frecuencyPaid> | <income> | <department> | <district> | <neighborhood> | <country> | <dependsNumber> | <phoneType> | <numberPhoneAdditional> | <docCreationDate> | <docFinalDate> | <companyName> | <employStartDate> | <occupation> | <streetWork> | <phoneTypeWork> | <streetWork> | <residentStatus> | <nameReference1> | <relationship1> | <typePhonereference1> | <numberReference1> | <nameReference2> | <relationship2> | <typePhonereference2> | <numberReference2> |
    Given the user into EMMA "<country>" with the credentials
    And select the country
    When fill out customer data
    And fill out additional information of the customer
    And fill out work information
    And fill out additional home information
    And fill out information about references
    And upload the documents for the "<creditLine>"
    Then validate the creation of the application "<message>"
    Examples:
      | country  | typeId              | documentId | creditLine                    | firstName | secondName | firstLastName | secondLastName | gender    | civilStatus | birthDate  | numberMovil | email             | sourceIncome        | frecuencyPaid | income   | department | district | neighborhood | dependsNumber | phoneType      | numberPhoneAdditional | companyName | employStartDate | occupation      | streetWork | phoneTypeWork          | residentStatus | nameReference1 | relationship1 | typePhonereference1       | numberReference1 | nameReference2 | relationship2 | typePhonereference2       | numberReference2 | message                                                 | docCreationDate | docFinalDate |
      | Paraguay | Cédula de Identidad | 1199217    | Línea de crédito microcrédito | David     | Jhon       | Campos        | Vega           | Masculino | Soltero     | 2000-MAY-7 | 0982326326  | prueba8@gmail.com | Dependiente con IPS | Mensual       | 15000000 | ASUNCION   | ASUNCION | SAJONIA      | 0             | Teléfono móvil | 0981326326            | Unicomer    | 2020-MAY-7      | Administrador/a | Calle 5    | Teléfono móvil trabajo | Propia         | Madre Cliente  | Madre         | Teléfono móvil referencia | 0984563623       | Padre Cliente  | Padre         | Teléfono móvil referencia | 0981236536       | ¡La aplicación al crédito ha sido completada con éxito! | 2020-MAY-7      | 2026-MAY-7   |

  @ExampleListwithExcel
  Scenario: Test for List
    When Test List
