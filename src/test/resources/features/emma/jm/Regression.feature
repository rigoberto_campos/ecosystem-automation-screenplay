@Regression
Feature: Regression CashLoan new client

  @EmmaNewClientForm
  Scenario Outline: 1. New credit application Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   | firstName   | secondName   | firstLastName   | secondLastName   | numberMovil   | email   | birthDate   | gender   | dependsNumber   | marriedStatus   | address   | frecuencyPaid   | income   | occupation   | bankendMonths   | jobMainActivity   | department   | district   | residentStatus   | monthsHome   | employStartDate   |
      | <typeId> | <documentId> | <firstName> | <secondName> | <firstLastName> | <secondLastName> | <numberMovil> | <email> | <birthDate> | <gender> | <dependsNumber> | <marriedStatus> | <address> | <frecuencyPaid> | <income> | <occupation> | <bankendMonths> | <jobMainActivity> | <department> | <district> | <residentStatus> | <monthsHome> | <employStartDate> |
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When admin user try to create new client
    And validate the document ID dose not exist
    When admin user enter basic information from the client
    Examples:
      | country | typeId | documentId | firstName | secondName | firstLastName | secondLastName | numberMovil | email | birthDate  | gender | dependsNumber | marriedStatus  | address                | frecuencyPaid | income  | occupation     | bankendMonths | jobMainActivity | department  | district | residentStatus | monthsHome | employStartDate |
      | Jamaica | TRN    | 989800042  | Jhon      | Cena       | deep          | Cartagena      | 72723331    | r@g.c | 1990-MAY-7 | Male   | 0             | Married        | This is the address 10 | Weekly        | 2000000 | Business Owner | 64            | Salary          | Hanover     | Ramble   | Owner          | 90         | 2020-MAY-7      |

  @WFTimeMonitoring
  Scenario Outline: 2. Reassign new client ticket
    Given User navigate into Workflow "<country>" with the credentials
    And Select the country
    Then Select "<menu>" menu in Workflow
    Then Assign request in workflow with client document "<documentId>"
    Examples:
      | country | documentId | menu            |
      | Jamaica | 989800042  | Time Monitoring |

  @RequestTracking
  Scenario Outline: 3. Approve new client request
    Given User navigate into Workflow "<country>" with the credentials
    And Select the country
    Then Select "<menu>" menu in Workflow
    When Approved client request with document "<trn>"
    Examples:
      | country  | trn       | menu             |
      | Jamaica  | 989800042 | Request Tracking |

  @NewLoanQuoter
  Scenario Outline: 4. New loan quoter Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   |loanAmount  |addCpi  |
      | <typeId> | <documentId> |<loanAmount>|<addCpi>|
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When the user click on loan quater menu
    When validate the document TRN and insert loans values
    Then validate the loan quoter of the application "<message>"

    Examples:
      | country | typeId | documentId |loanAmount  |addCpi|message                                                        |
      | Jamaica | TRN    | 989800042  |35000.00     |Yes   |We are processing your quote, this might take several minutes  |

  @CreditApplication
  Scenario Outline: 5. New credit application status Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   |
      | <typeId> | <documentId> |
    Given DataTable request data
      |typeRequest   |selectPromotion  |termMonths  |store  |csrNumber  |
      |<typeRequest> |<selectPromotion>|<termMonths>|<store>|<csrNumber>|
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When the user click on "<menu>" menu
    When validate the documentId TRN and insert credit values
    And the user enter basic information from the request
    Then validate the credit request of the application "<message>"

    Examples:
      | country | typeId | documentId |typeRequest  |selectPromotion  |termMonths  |store                |csrNumber  |message    | menu                      |
      | Jamaica | TRN    | 989800042  |Loan Status  |Cash loan        | 9          | LUCKY DOLLAR MAY PEN| 9902996   |Request ID | Credit application status |

  @RequestTracking
  Scenario Outline: 6. Approve loan request
    Given User navigate into Workflow "<country>" with the credentials
    And Select the country
    Then Select "<menu>" menu in Workflow
    When Approved loan request with document "<trn>"
    Examples:
      | country  | trn       | menu             |
      | Jamaica  | 989800042 | Request Tracking |

  @PostSaleEvaluation
  Scenario Outline: 7. New credit application post sale evaluation Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   |
      | <typeId> | <documentId> |
    Given DataTable request data
      |typeRequest   |
      |<typeRequest> |
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When the user click on "<menu>" menu
    When validate the documentId TRN and insert credit values
    Then validate the credit post sale evaluation of the application "<message>"
    And validate uploaded documents

    Examples:
      | country | typeId | documentId |typeRequest           |message                                           | menu                      |
      | Jamaica | TRN    | 989800042  |Post-sale Evaluation  |You can download the customer's billing documents.| Credit application status |

  @NewConsultClient
  Scenario Outline: 8. Test consult client JM uat
    Given User navigate into Mambu "<country>" with the credentials
    When consult the "<documentId>" exist values
    And select the "<option>" of consult
    Then validate of the application with "<status>"

    Examples:
      | country |documentId|option             |status  |
      | Jamaica |989800042 |Cash Loan Bluestart|Approved|

  @Cosacs
  Scenario: 9. Test Rest Disbursement
    When Disbursement Cosacs

  @NewConsultClient
  Scenario Outline:  Test consult client JM uat
    Given User navigate into Mambu "<country>" with the credentials
    When consult the "<documentId>" exist values
    And select the "<option>" of consult
    Then validate of the application with "<status>"

    Examples:
      | country |documentId|option             |status  |
      | Jamaica |989800042 |Cash Loan Bluestart|J$      |
