@CashLoan
Feature: Emma New loan quoter JM uat

  @NewLoanQuoter
  Scenario Outline: New loan quoter Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   |loanAmount  |addCpi  |
      | <typeId> | <documentId> |<loanAmount>|<addCpi>|
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When the user click on loan quater menu
    When validate the document TRN and insert loans values
    Then validate the loan quoter of the application "<message>"

    Examples:
      | country | typeId | documentId |loanAmount  |addCpi|message                                                        |
      | Jamaica | TRN    | 989800009  |35000.00     |Yes   |We are processing your quote, this might take several minutes  |

  @loanStatus
  Scenario Outline: New credit application loan status Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   |
      | <typeId> | <documentId> |
    Given DataTable request data
      |typeRequest   |selectPromotion  |termMonths  |store  |csrNumber  |
      |<typeRequest> |<selectPromotion>|<termMonths>|<store>|<csrNumber>|
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When the user click on "<menu>" menu
    When validate the documentId TRN and insert credit values
    And the user enter basic information from the request
    Then validate the credit request of the application "<message>"

    Examples:
      | country | typeId | documentId |typeRequest  |selectPromotion  |termMonths  |store                |csrNumber  |message    | menu                      |
      | Jamaica | TRN    | 989800009  |Loan Status  |Cash loan        | 9          | LUCKY DOLLAR MAY PEN| 9902996   |Request ID | Credit application status |

  @PostSaleEvaluation
  Scenario Outline: New credit application post sale evaluation Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   |
      | <typeId> | <documentId> |
    Given DataTable request data
      |typeRequest   |
      |<typeRequest> |
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When the user click on "<menu>" menu
    When validate the documentId TRN and insert credit values
    Then validate the credit post sale evaluation of the application "<message>"
    And validate uploaded documents

    Examples:
      | country | typeId | documentId |typeRequest           |message                                           | menu                      |
      | Jamaica | TRN    | 989900001  |Post-sale Evaluation  |You can download the customer's billing documents.| Credit application status |