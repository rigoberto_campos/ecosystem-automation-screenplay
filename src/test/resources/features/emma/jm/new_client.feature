@NewClientFlow
Feature: Emma New client JM uat

  @EmmaNewClientForm
  Scenario Outline: New credit application Emma JM uat
    Given DataTable customer data
      | typeId   | documentId   | firstName   | secondName   | firstLastName   | secondLastName   | numberMovil   | email   | birthDate   | gender   | dependsNumber   | marriedStatus   | address   | frecuencyPaid   | income   | occupation   | bankendMonths   | jobMainActivity   | department   | district   | residentStatus   | monthsHome   | employStartDate   |
      | <typeId> | <documentId> | <firstName> | <secondName> | <firstLastName> | <secondLastName> | <numberMovil> | <email> | <birthDate> | <gender> | <dependsNumber> | <marriedStatus> | <address> | <frecuencyPaid> | <income> | <occupation> | <bankendMonths> | <jobMainActivity> | <department> | <district> | <residentStatus> | <monthsHome> | <employStartDate> |
    Given the user into uat EMMA "<country>" with the credentials
    And select the country uat emma
    When admin user try to create new client
    And validate the document ID dose not exist
    When admin user enter basic information from the client
    Examples:
      | country | typeId | documentId | firstName | secondName | firstLastName | secondLastName | numberMovil | email | birthDate  | gender | dependsNumber | marriedStatus  | address                | frecuencyPaid | income  | occupation     | bankendMonths | jobMainActivity | department  | district | residentStatus | monthsHome | employStartDate |
      | Jamaica | TRN    | 989800024  | Jhon      | Cena       | deep          | Cartagena      | 72723331    | r@g.c | 1990-MAY-7 | Male   | 0             | Married        | This is the address 10 | Weekly        | 2000000 | Business Owner | 64            | Salary          | Hanover     | Ramble   | Owner          | 90         | 2020-MAY-7      |

  @WFTimeMonitoring
  Scenario Outline: Login into Workflow
    Given User navigate into Workflow "<country>" with the credentials
    And Select the country
    Then Select "<menu>" menu in Workflow
    Then Assign request in workflow with client document "<documentId>"
    Examples:
      | country | documentId | menu            |
      | Jamaica | 989800024  | Time Monitoring |

  @RequestTracking
  Scenario Outline: Login into Workflow
    Given User navigate into Workflow "<country>" with the credentials
    And Select the country
    Then Select "<menu>" menu in Workflow
    Then Approved client request with document "<trn>"
    Examples:
      | country  | trn       | menu             |
      | Jamaica  | 989800024 | Request Tracking |