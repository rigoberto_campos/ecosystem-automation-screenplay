@MambuJm
Feature: Mambu Login

  @NewClient
  Scenario Outline: Test Login in Jamaica
    Given User navigate into Mambu "<country>" with the credentials

    Examples:
      | country |
      | Jamaica   |
