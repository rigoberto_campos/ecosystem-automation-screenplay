@MambuJm
Feature: Mambu consult client JM uat

  @NewConsultClient
  Scenario Outline: Test consult client JM uat
    Given User navigate into Mambu "<country>" with the credentials
    When consult the "<documentId>" exist values
    And select the "<option>" of consult
    Then validate of the application with "<status>"


    Examples:
      | country |documentId|option             |status  |
      | Jamaica |989900003 |Cash Loan Bluestart|Approved|