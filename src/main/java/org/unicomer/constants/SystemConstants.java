package org.unicomer.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class SystemConstants {
    private SystemConstants(){
        if(true){

        }

    }
    public static final String MAMBU_URL = "MAMBU.URL";
    public static final String MAMBU_USERNAME = "MAMBU.USERNAME";
    public static final String MAMBU_PASSWORD = "MAMBU.PASSWORD";
    public static final String COUNTRY = "COUNTRY";
    public static final String EMMA_USERNAME = "EMMA.USERNAME";
    public static final String EMMA_PASSWORD = "EMMA.PASSWORD";
    public static final String EMMA_URL = "EMMA.URL";
    public static final String WORKF_URL = "WORKF.URL";
    public static String WORKF_USERNAME = "WORKF.USERNAME";
    public static String WORKF_PASSWORD = "WORKF.PASSWORD";
    public static final String LANGUAGE = "LANGUAGE";
    public static final String EXCEL_PATH = "src/test/resources/files/data/CustomerEmma.xlsx";
    public static final String DOCUMENT_PATH = "src\\test\\resources\\files\\dni.PNG";

    public static final String DRIVER_TYPE = "webdriver.chrome.driver";

    public static final String DRIVER_CHROME = "src/test/resources/drivers/chromedriver.exe";

    public static final List<String> CAPABILITY_ARGUMENTS = new ArrayList<String>(Arrays.asList("test-type", "no-sandbox",
            "ignore-certificate-errors", "start-maximized", "--incognito", "disable-infobars", "disable-gpu",
            "disable-default-apps", "disable-popup-blocking", "--remote-allow-origins=*"));

}
