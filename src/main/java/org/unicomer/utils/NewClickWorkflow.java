package org.unicomer.utils;

import net.serenitybdd.screenplay.*;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;

public class NewClickWorkflow implements Performable {
    private final Target element;
    int intentosMaximos=4;
    private final Target spinner;
    int intento=1;
    public NewClickWorkflow(Target element,Target spinner){
        this.element=element;
        this.spinner=spinner;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        for(int retry = 0;retry<intentosMaximos;retry++)  {
            try{
                actor.attemptsTo(Click.on(element));
                return;
            }catch(ElementClickInterceptedException | StaleElementReferenceException | TimeoutException e){
                System.out.println("Target no encontrado"+ element.getName());
                Wait.to(1);
                WaitUntil.the(spinner, WebElementStateMatchers.isNotVisible());

            }
        }
    }

    public static Task onTargetWithRetries(Target element, Target spinner){
        return Task.where("{0} retries clicking on "+ element.getName(),new NewClickWorkflow(element,spinner));

    }
}
