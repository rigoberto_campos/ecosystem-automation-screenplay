package org.unicomer.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.unicomer.constants.SystemConstants;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class DriverManager {

    private static DriverManager instance;
    private RemoteWebDriver remoteDriver;
    private ChromeDriver chromeDriver;

    private DriverManager(String driver){
        switch (driver){
            case "REMOTE":
                browserOptions();
                break;
            default:
                chromeOptions();
        }
    }

    private void browserOptions(){
        ChromeOptions browserOptions = new ChromeOptions();
        browserOptions.setPlatformName("Windows 11");
        browserOptions.setBrowserVersion("latest");
        browserOptions.addArguments("start-maximized");
        browserOptions.addArguments("incognito");
        browserOptions.addArguments("remote-allow-origins=*");
        browserOptions.addArguments("--disable-gpu", "--remote-allow-origins=*", "--no-sandbox");
        browserOptions.setImplicitWaitTimeout(Duration.ofSeconds(30));
        Map<String, Object> sauceOptions = new HashMap<>();
        sauceOptions.put("username", "rigoberto_campos");
        sauceOptions.put("accessKey", "245c58f9-837c-49a1-a810-b78e30e84e73");
        sauceOptions.put("build", "selenium-build-WF84J");
        sauceOptions.put("name", "testing Sauce");
        sauceOptions.put("screenResolution", "1920x1200");
        browserOptions.setCapability("sauce:options", sauceOptions);

        URL url = null;
        try {
            url = new URL("https://ondemand.us-west-1.saucelabs.com:443/wd/hub");
            this.remoteDriver = new RemoteWebDriver(url, browserOptions);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

    }

    public void chromeOptions(){
        System.setProperty(SystemConstants.DRIVER_TYPE, SystemConstants.DRIVER_CHROME);
        ChromeOptions co = new ChromeOptions();
        co.addArguments(SystemConstants.CAPABILITY_ARGUMENTS);
        co.setImplicitWaitTimeout(Duration.ofSeconds(90));
        WebDriverManager.chromedriver().setup();
        this.chromeDriver = new ChromeDriver(co);

    }

    private void fluentWaitDriver(){

    }

    public static synchronized DriverManager getInstance(String driver){
        if(instance == null){
            instance = new DriverManager(driver);
        }
        return instance;
    }

    public static DriverManager getInstance(){
        return getInstance("");
    }

    public RemoteWebDriver getRemoteDriver(){
        return remoteDriver;
    }

    public ChromeDriver getChromeDriver(){
        return chromeDriver;
    }

}
