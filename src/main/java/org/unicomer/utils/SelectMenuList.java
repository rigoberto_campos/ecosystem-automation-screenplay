package org.unicomer.utils;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SelectMenuList implements Interaction  {
    private final String option;
    public SelectMenuList(String option){
        this.option = option;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(Target.the("Select Menu").located(By.xpath("(//div[contains(text(),'"+option+"')])[1]"))));
    }
    public static SelectMenuList since(String option){
        return new SelectMenuList(option);
    }
}
