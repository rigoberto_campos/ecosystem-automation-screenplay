package org.unicomer.utils;

import net.serenitybdd.screenplay.Actor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.unicomer.constants.SystemConstants;
import org.unicomer.models.Customer;
import org.unicomer.models.Request;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class UtilsManager {

    private static UtilsManager instance;

    private String id_trn;

    private UtilsManager(){
        setTRN();
    }
    private String getRandomId(int min, int max){

        Integer randomInt = (int)Math.floor(Math.random() * (max - min + 1) + min);
        return randomInt.toString();
    }

    public static synchronized UtilsManager getInstance(){
        if(instance == null){
            instance = new UtilsManager();
        }
        return instance;
    }

    public Map readExcel(int row){
        try {
            String excelPath = SystemConstants.EXCEL_PATH;
            FileInputStream file = new FileInputStream(new File(excelPath));
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Cell> keyIterator =  sheet.getRow(0).cellIterator();
            int x = 0;
            Map dataFile = new HashMap<>();

            while (keyIterator.hasNext())
            {
                String key = keyIterator.next().getStringCellValue();
                String value =  sheet.getRow(row).getCell(x).getStringCellValue();

                dataFile.put(key,value);
                x = x + 1;

            }
            file.close();
            return dataFile;
        } catch (RuntimeException | IOException e) {
            //throw new RuntimeException(e);
            return null;
        }
    }

    public String getEmail(){
        String format="yyyy-MM-dd HH:mm:ss";
        DateTimeFormatter date = DateTimeFormatter.ofPattern(format);
        LocalDateTime now = LocalDateTime.now();
        String fecha = date.format(now);
        fecha=fecha.replace("-","");
        fecha=fecha.replace(" ","");
        fecha=fecha.replace(":","");
        return "correo"+fecha+"@gmail.com";
    }

    private void setTRN(){
        id_trn = getRandomId(100000000, 999999999);
    }

    public void setTRN(String document){
        id_trn = document;
    }


    public String getTRN(){
        return id_trn;
    }

    public void setPassport(){
        this.id_trn = this.id_trn + "9";
    }

    public String getPathDocument(){
        String pathDocument = System.getProperty("user.dir");
        return pathDocument+"\\"+SystemConstants.DOCUMENT_PATH;
    }

    public String readBodyFile(String path){
        String line="";
        try {
            File text = new File(path);
            //Creating Scanner instance to read File in Java
            Scanner scnr = new Scanner(text);

            while(scnr.hasNextLine()){
                line = scnr.nextLine();
            }
            scnr.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return line;
    }

    public String replaceBodyVar(String body){
        String newBody = body;
        restVar.put("${password}", "12345678**1*$");
        restVar.put("${username}", "rigoberto.campos");
        for(Map.Entry<String, String> entry : restVar.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            newBody = newBody.replace(key, value);
            System.out.println(key + "=" + value + " ");

        }
        return newBody;
    }

    private HashMap<String,String> restVar=new HashMap<>();

    public void addRestVaribles(String k,String v){
        restVar.put(k,v);
    }

    public String getRestVariable(String k){
        return restVar.get(k);
    }

    private Customer customer;

    public void setCustomer(Customer customer){
        this.customer = customer;
    }

    public Customer getCustomer(){

        return this.customer;
    }

    public void setActor(Actor admin) {
        this.actor = admin;
    }

    private Actor actor;
    public Actor getActor(){
        return this.actor;
    }

    private Request request;

    public void setRequest(Request request){
        this.request = request;
    }

    public Request getRequest(){

        return this.request;
    }
}
