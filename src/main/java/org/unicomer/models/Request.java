package org.unicomer.models;

import java.util.Map;

public class Request {

    private String typeRequest;
    private String selectPromotion;
    private String termMonths;
    private String store;
    private String csrNumber;
    public Request(Map<String, String> entry) {
        this.typeRequest = entry.get("typeRequest");
        this.selectPromotion = entry.get("selectPromotion");
        this.termMonths = entry.get("termMonths");
        this.store = entry.get("store");
        this.csrNumber = entry.get("csrNumber");
    }

    public String getTypeRequest() {
        return this.typeRequest;
    }

    public String getSelectPromotion() {
        return selectPromotion;
    }

    public String getTermMonths() {
        return termMonths;
    }

    public String getStore() {
        return store;
    }

    public String getCsrNumber() {
        return csrNumber;
    }
}
