package org.unicomer.models;

import org.unicomer.utils.UtilsManager;

import java.util.Map;

public class Customer {
    public String typeId;
    public String documentId;
    private String creditLine;
    private String firstName;
    private String secondName;
    private String firstLastName;
    private String secondLastName;
    public String gender;
    private String civilStatus;
    private String birthDate;
    private String numberMovil;
    private String email;
    private String sourceIncome;
    private String frecuencyPaid;
    private String income;
    private String department;
    private String district;
    private String neighborhood;
    private String marriedStatus;
    private String year;
    private String month;
    private String day;
    private String yearJ;
    private String monthJ;
    private String dayJ;
    private String fullName;
    private String docCreationDate;
    private String docFinalDate;
    private String dcYear;
    private String dcMonth;
    private String dcDay;
    private String dfYear;
    private String dfMonth;
    private String dfDay;
    private String dependsNumber;
    private String address;
    private String occupation;
    private String bankendMonths;
    private String jobMainActivity;
    private String residentStatus;
    private String monthsHome;
    private String employStartDate;
    private String country;

    private String phoneType;
    private String companyName;
    private String streetWork;
    private String phoneTypeWork;

    private String nameReference1;
    private String relationship1;
    private String typePhonereference1;
    private String numberReference1;
    private String nameReference2;
    private String relationship2;
    private String loanAmount;
    private String addCpi;

    public Customer(Map<String, String> entry) {
        this.typeId = entry.get("typeId");
        this.documentId = entry.get("documentId");
        this.creditLine = entry.get("creditLine");
        this.firstName = entry.get("firstName");
        this.secondName = entry.get("secondName");
        this.firstLastName = entry.get("firstLastName");
        this.secondLastName = entry.get("secondLastName");
        this.gender = entry.get("gender");
        this.civilStatus = entry.get("civilStatus");
        this.birthDate = entry.get("birthDate");
        this.numberMovil = entry.get("numberMovil");
        this.email = entry.get("email");
        this.sourceIncome = entry.get("sourceIncome");
        this.frecuencyPaid = entry.get("frecuencyPaid");
        this.income = entry.get("income");
        this.department = entry.get("department");
        this.district = entry.get("district");
        this.neighborhood = entry.get("neighborhood");
        this.docCreationDate = entry.get("docCreationDate");
        this.docFinalDate = entry.get("docFinalDate");
        this.dependsNumber = entry.get("dependsNumber");
        this.marriedStatus = entry.get("marriedStatus");
        this.address = entry.get("address");
        this.occupation = entry.get("occupation");
        this.bankendMonths = entry.get("bankendMonths");
        this.jobMainActivity = entry.get("jobMainActivity");
        this.residentStatus = entry.get("residentStatus");
        this.monthsHome = entry.get("monthsHome");
        this.employStartDate = entry.get("employStartDate");
        this.country = entry.get("country");
        this.phoneType = entry.get("phoneType");
        this.companyName = entry.get("companyName");
        this.streetWork = entry.get("streetWork");
        this.phoneTypeWork = entry.get("phoneTypeWork");
        this.nameReference1 = entry.get("nameReference1");
        this.relationship1 = entry.get("relationship1");
        this.typePhonereference1 = entry.get("typePhonereference1");
        this.numberReference1 = entry.get("numberReference1");
        this.nameReference2 = entry.get("nameReference2");
        this.relationship2 = entry.get("relationship2");
        this.loanAmount = entry.get("loanAmount");
        this.addCpi = entry.get("addCpi");
        setFullname();
        setYearMonthDay();
        setDocumentCreationD();
        setDocumentFinalD();
        setEmail();
        setEmployStartDate();
    }

    @Override
    public String toString() {
        return "Customer{" + "typeId='" + typeId  + ", documentId='" + documentId + ", fullName='" + fullName + '}';
    }

    public void setEmail(){
        email = UtilsManager.getInstance().getEmail();
    }

    private void setFullname(){
        if (firstName==(null)) {
            return;
        }
        fullName = firstName + " " + secondName + " " + firstLastName + " " + secondLastName;
    }

    private void setYearMonthDay(){
        if (birthDate==(null)) {
            return;
        }
        this.year = birthDate.split("-")[0];
        this.month = birthDate.split("-")[1];
        this.day = birthDate.split("-")[2];
    }

    private void setEmployStartDate(){
        if(employStartDate == null){
            return;
        }
        this.yearJ = employStartDate.split("-")[0];
        this.monthJ = employStartDate.split("-")[1];
        this.dayJ = employStartDate.split("-")[2];
    }

    private void setDocumentCreationD(){
        if(docCreationDate == null) return;
        this.dcYear = docCreationDate.split("-")[0];
        this.dcMonth = docCreationDate.split("-")[1];
        this.dcDay = docCreationDate.split("-")[2];
    }

    private void setDocumentFinalD(){
        if(docFinalDate == null) return;
        this.dfYear = docFinalDate.split("-")[0];
        this.dfMonth = docFinalDate.split("-")[1];
        this.dfDay = docFinalDate.split("-")[2];
    }

    public String getFullName() {
        return fullName;
    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }
    public String getDayJ() {
        return dayJ;
    }
    public String getYearJ() {
        return yearJ;
    }

    public String getMonthJ() {
        return monthJ;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public String getCreditLine() {
        return creditLine;
    }

    public String getGender() {
        return gender;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public String getNumberMovil() {
        return numberMovil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSourceIncome() {
        return sourceIncome;
    }

    public void setSourceIncome(String sourceIncome) {
        this.sourceIncome = sourceIncome;
    }

    public String getFrecuencyPaid() {
        return this.frecuencyPaid;
    }

    public String getIncome() {
        return income;
    }

    public String getDepartment() {
        return department;
    }

    public String getDistrict() {
        return district;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public String getDcYear() {
        return dcYear;
    }

    public String getDcMonth() {
        return dcMonth;
    }

    public String getDcDay() {
        return dcDay;
    }

    public String getDfYear() {
        return dfYear;
    }

    public String getDfMonth() {
        return dfMonth;
    }

    public String getDfDay() {
        return dfDay;
    }

    public String getDependsNumber(){
        return this.dependsNumber;
    }
    public String getMarriedStatus(){
        return this.marriedStatus;
    }
    public String getAddress(){
        return this.address;
    }

    public String getOccupation(){
        return this.occupation;
    }
    public String getBankendMonths() {
        return this.bankendMonths;
    }

    public String getJobMainActivity(){
        return this.jobMainActivity;
    }
    public String getResidentStatus(){
        return this.residentStatus;
    }

    public String getMonthsHome(){
        return this.monthsHome;
    }

    public String getCountry(){
        return this.country;
    }

    public String getPhoneType(){
        return this.phoneType;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public String getStreetWork(){
        return this.streetWork;
    }

    public String getPhoneTypeWork() {
        return this.phoneTypeWork;
    }

    public String getNameReference1(){
        return this.nameReference1;
    }

    public String getRelationship1() {
        return this.relationship1;
    }

    public String getTypePhonereference1() {
        return this.typePhonereference1;
    }

    public String getNumberReference1() {
        return this.numberReference1;
    }

    public String getNameReference2() {
        return this.nameReference2;
    }

    public String getRelationship2() {
        return this.relationship2;
    }

    public String getLoanAmount() {return loanAmount;}
    public String getAddCpi() {return addCpi;}

}
